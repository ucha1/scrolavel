<?php

namespace C4S;

use Illuminate\Support\Collection;

/**
 * @method static Accounttype Accounttype()
 * @method static Affiliate Affiliate()
 * @method static Animatedgifcapturetimes Animatedgifcapturetimes()
 * @method static Auth Auth()
 * @method static Badstores Badstores()
 * @method static Badwords Badwords()
 * @method static Banners Banners()
 * @method static BannedBadWords BannedBadWords()
 * @method static Bundle Bundle()
 * @method static C4slive C4slive()
 * @method static Cache Cache()
 * @method static Captcha Captcha()
 * @method static Category Category()
 * @method static Cart Cart()
 * @method static Clipformats Clipformats()
 * @method static Clippreviewcapturetimes Clippreviewcapturetimes()
 * @method static Clippreviewcapturetimesv2 Clippreviewcapturetimesv2()
 * @method static Clipreview Clipreview()
 * @method static Clips Clips()
 * @method static Clipservicesconf Clipservicesconf()
 * @method static Clipsiteservicesconf Clipsiteservicesconf()
 * @method static Cms Cms()
 * @method static Contentmanagement Contentmanagement()
 * @method static Countries Countries()
 * @method static Customer Customer()
 * @method static Dashboard Dashboard()
 * @method static Details Details()
 * @method static Dmca Dmca()
 * @method static Domains Domains()
 * @method static Download Download()
 * @method static Email Email()
 * @method static Faq Faq()
 * @method static Feeds Feeds()
 * @method static Ftp Ftp()
 * @method static Futureactivationqueue Futureactivationqueue()
 * @method static Geo Geo()
 * @method static Geoip Geoip()
 * @method static Health Health()
 * @method static Hermes Hermes()
 * @method static Hiddenproducers Hiddenproducers()
 * @method static Highwinds Highwinds()
 * @method static I4s I4s()
 * @method static I4sdataupdate I4sdataupdate()
 * @method static Imageformats Imageformats()
 * @method static Images Images()
 * @method static Keywords Keywords()
 * @method static Linkchecker Linkchecker()
 * @method static Listpage Listpage()
 * @method static Merchant Merchant()
 * @method static Messages Messages()
 * @method static Migrations Migrations()
 * @method static Monitoringsystem Monitoringsystem()
 * @method static Noncustomer Noncustomer()
 * @method static Oauthtokens Oauthtokens()
 * @method static Owners Owners()
 * @method static Passwordtoken Passwordtoken()
 * @method static Payoneeraccounts Payoneeraccounts()
 * @method static Payouts Payouts()
 * @method static Permissions Permissions()
 * @method static Ping Ping()
 * @method static Producercats Producercats()
 * @method static Producerclipcats Producerclipcats()
 * @method static Producercliphash Producercliphash()
 * @method static Producerclips Producerclips()
 * @method static Producerclipsite Producerclipsite()
 * @method static Producermessage Producermessage()
 * @method static Producers Producers()
 * @method static Producervideosite Producervideosite()
 * @method static Rabbit Rabbit()
 * @method static Rapidapi Rapidapi()
 * @method static Rectalogist Rectalogist()
 * @method static Regex Regex()
 * @method static Removedcliphash Removedcliphash()
 * @method static Reportgenerator Reportgenerator()
 * @method static Reports Reports()
 * @method static Rolloverimages Rolloverimages()
 * @method static Settings Settings()
 * @method static Signupfields Signupfields()
 * @method static Signupselectionsoptions Signupselectionsoptions()
 * @method static Signupsteps Signupsteps()
 * @method static Slack Slack()
 * @method static Socialbuttons Socialbuttons()
 * @method static Socialbuttonsdisabled Socialbuttonsdisabled()
 * @method static States States()
 * @method static Storagesync Storagesync()
 * @method static Stream Stream()
 * @method static Studio Studio()
 * @method static Suggestions Suggestions()
 * @method static Support Support()
 * @method static Tempsignup Tempsignup()
 * @method static Token Token()
 * @method static Topclips Topclips()
 * @method static Traffic Traffic()
 * @method static Transaction Transaction()
 * @method static Transcoding Transcoding()
 * @method static Transcodingqueue Transcodingqueue()
 * @method static Tweet Tweet()
 * @method static Twitterserviceconf Twitterserviceconf()
 * @method static Twofactor Twofactor()
 * @method static Unpostedfile Unpostedfile()
 * @method static Uploader Uploader()
 * @method static Url Url()
 * @method static Utilities Utilities()
 * @method static V4s V4s()
 * @method static V4sdataupdate V4sdataupdate()
 * @method static Videos Videos()
 * @method static CommerceErrorCode CommerceErrorCode()
 * @method static Batchemail Batchemail()
 * @method static Clipcashaccounts Clipcashaccounts()
 * @method static Exchange Exchange()
 * @method static WLUserClips WLUserClips()
 * @method static WLCustomers WLCustomers()
 * @method static WLStudios WLStudios()
 * @method static WLClips WLClips()
 * @method static WLSites WLSites()
 * @method static WLModels WLModels()
 * @method static Taxlocation Taxlocation()
 * @method static Taxratesmeta Taxratesmeta()
 * @method static RightSidePromotion RightSidePromotion()
 * @method static Gdpr Gdpr()
 * @method static ProducerBannerFileName ProducerBannerFileName()
 * @method static Tracker Tracker()
 * @method static ZipCodeBase ZipCodeBase()
 * @method static ProducerClipMetaData ProducerClipMetaData()
 **/
class Scrotis
{
    public static $method = [];
    public static $param;

    static function __callStatic($method, $args)
    {
        self::$method[0] = $method;
        $className       = __NAMESPACE__."\\$method";
        return new $className();
    }

    public function __call($name, $arg)
    {
        self::$method[1] = $name;
        self::$param     = $arg;
        return $this;
    }

    /**
     * @param  string  $method
     * @param  string  $endpoint
     * @param  array  $obj
     * @return Collection
     **/
    public static function exec(string $method, string $endpoint, array $obj = [])
    {
        return collect(API::exec($method, $endpoint, $obj));
    }

    /**
     * @param $host
     * @return Scrotis
     **/
    public function setHost(string $host): Scrotis
    {
        API::setHost($host);
        return $this;
    }

    /**
     * @return string
     **/
    public function getHost(): string
    {
        return API::getHost();
    }

    /**
     * @return $this
     **/
    public function noCache(): Scrotis
    {
        API::withHeaders(['Cache-Control: no-cache']);
        return $this;
    }

    /**
     * @param  array  $arr
     * @return $this
     **/
    public function withHeaders(array $arr): Scrotis
    {
        API::withHeaders($arr);
        return $this;
    }

    /**
     * @param $param  ["username" => "", "password" => "", "token" => ""]
     * @return $this
     **/
    public function withToken(array $param): Scrotis
    {
        API::withToken($param);
        return $this;
    }


    /**
     * @return string
     **/
    private function generateUrl(): string
    {
        $param = collect(self::$param);
        $url   = '/'.static::$method[0];

        if ($param->isEmpty()) {
            $url .= '/:nid/'.static::$method[1];
        } else {
            if ($first = $param->first()) {
                $url .= '/'.$first;
            }

            if (is_array($second = $param->slice(1)->get(1))) {
                $arr = [];
                foreach ($second as $key => $key_value) {
                    $arr[] = urlencode($key).'='.urlencode($key_value);
                }

                $url .= '/'.static::$method[1].'?'.implode('&', $arr);
            } else {
                $url .= '/'.static::$method[1];
            }
        }

        return $url;
    }

    /**
     * @param  array  $data
     * @return Collection
     **/
    public function get(array $data = [])
    {
        return collect(API::get($this->generateUrl(), $data));
    }


    /**
     * Get data from multiple endpoints at once
     * [$response1,$response2] = Scrotis::getMulti(['endpoint1','endpoint2'],[])
     *
     * @param  array  $endpoints
     * @param  array  $options
     * @return Collection
     **/
    public function getMulti(array $endpoints, array $options = [])
    {
        return collect(API::poolGet($endpoints, $options));
    }

    /**
     * POST data for multiple endpoints at once
     * [$response1,$response2] = Scrotis::postMulti([
     *      ['/endpoint1',['CategoryID' => '13','ProducerID' => '1587']]
     *      ['/endpoint2',['CategoryID' => '13','ProducerID' => '1587']]
     * ])
     *
     * @param  array  $endpoints
     * @param  array  $options
     * @return Collection
     **/
    public function postMulti(array $endpoints, array $options = [])
    {
        return collect(API::poolPost($endpoints, $options));
    }

    /**
     * @param  array  $data
     * @return Collection
     **/
    public function post(array $data = [])
    {
        return collect(API::post($this->generateUrl(), $data));
    }

    /**
     * @param  array  $data
     * @return Collection
     **/
    public function put(array $data = [])
    {
        return collect(API::put($this->generateUrl(), $data));
    }

    /**
     * @param  array  $data
     * @return Collection
     **/
    public function delete(array $data = [])
    {
        return collect(API::delete($this->generateUrl(), $data));
    }
}

/***************************************************************ALL SCROTIS CLASSES***************************************************************/

/**
 * @package Utilities
 * @method Scrotis monitorDownloads()
 * @method Scrotis getBadProducers()
 * @method Scrotis getStoreProducersBy() @param [str]
 * @method Scrotis insertIntoBadStore() @payload [ID, reason]
 * @method Scrotis getTwitterTokens() @param [ids]
 * @method Scrotis getTokenCredentials($ProducerID)
 * @method Scrotis deleteProducerTweeterTokenExpired($ProducerID)
 * @method Scrotis getProducerMerchantBy($ProducerID)
 * @method Scrotis getScheduledTweet()
 * @method Scrotis getProducerCanTweet($ProducerID)
 * @method Scrotis getProducerCanTweetForProducer($ProducerID)
 * @method Scrotis updateScheduledTweet($TweetScheduleID) @payload [Status, TwitterResponse]
 * @method Scrotis getTweetsToPost()
 * @method Scrotis findProducersWithTwitterOptionsEnabled()
 * @method Scrotis generateScheduledTweetsForProducer($ProducerID)
 * @method Scrotis getAllDayEvents()
 * @method Scrotis getNotAllDayEvents()
 * @method Scrotis getMerchantAccount()
 * @method Scrotis updateMarkAsTweeted($id)
 * @method Scrotis getTransactionsCountBy($cc_id) @payload [start_date]
 * @method Scrotis getTransactionsBy($cc_id) @payload [start_date]
 * @method Scrotis recentStudioTributes($ProducerID) @payload [price, interval, limit, service]
 * @method Scrotis recentStudioClipOrders($ProducerID) @payload [price, interval, limit, service]
 * @method Scrotis getTransactionsWithClip($ProducerID) @payload [price, interval, limit, service]
 * @method Scrotis getMonToSun()
 * @method Scrotis enterRecurringInDb($id)
 * @method Scrotis getEmails()
 * @method Scrotis insertLogToDB() @payload [cc, count, minute_interval_setting, trans_required_setting, error_status, username]
 * @method Scrotis getFirstClips()
 * @method Scrotis getBatchEmailEximLog()
 * @method Scrotis updateDateTimeProcessingStarted($BatchEmailEximLogQueryRequestID)
 * @method Scrotis updateDateTimeProcessingCompleted($BatchEmailEximLogQueryRequestID)
 * @method Scrotis clipDateBy()
 * @method Scrotis insertFirstClipAdded() @payload [SiteID, DateAndTimeFirstClipAdded, MerchantAccount, ReasonNotLinked]
 * @method Scrotis topSellingClips($ProducerID)
 * @method Scrotis getIDs()
 * @method Scrotis getAllIDs()
 * @method Scrotis deleteTopSellingClipBy($ProducerID)
 * @method Scrotis insertIntoTopSellingClips() @payload [ProducerID, ClipID, Title, SoldCount]
 * @method Scrotis getLargeOrdersWithoutDownloads()
 * @method Scrotis addDashboardFeed() @payload [DataElementName, DateTimeAdded, DashboardData]
 * @method Scrotis getBatchMailServers()
 * @method Scrotis getDomainLog()
 * @method Scrotis getBlockedDomainStatus()
 * @method Scrotis updateUnblockAll()
 * @method Scrotis updateDomainStatus() @payload [BatchEmail_DomainName, BatchEmail_DomainStatus, BatchEmail_BlockedTime]
 * @method Scrotis addBlockedDomains($param = 0 | 1) @payload [0 => [BatchEmail_DomainName, BatchEmail_DomainStatus, BatchEmail_BlockedTime, BatchEmail_UnblockedTime], 1 => [BatchEmailEximDomainLogID, DomainName, DateTimeAdded, LogInfo, MailServer]]
 * @method Scrotis updateLastPasswordChangeDate()
 * @method Scrotis getProdWithoutBanner()
 * @method Scrotis deleteProducerIdWithoutBannerFile($ProducerID)
 * @method Scrotis addProducersWithoutBannerFile() @payload [ProducerIDs]
 * @method Scrotis getTotalHitCount($ProducerID)
 * @method Scrotis getTotalHitCountDate($ProducerID)
 * @method Scrotis getTotalSales($ProducerID) @payload [date]
 * @method Scrotis addStatisticalTotals() @payload [ProducerID, TotalSales, TotalProducerCommission, TotalHitsPerDay, DataDat]
 * @method Scrotis getProducerIDs()
 * @method Scrotis getScrutinizeSiteIds()
 * @method Scrotis getDaysSinceLastUpdate($ProducerID)
 * @method Scrotis getTotalSalesSum($ProducerID)
 * @method Scrotis getTotalChargeback($ProducerID)
 * @method Scrotis getMissingCategories($ProducerID)
 * @method Scrotis getProdCommission()
 * @method Scrotis getCustomersBy($customer_id)
 * @method Scrotis getProducerById($ProducerID)
 * @method Scrotis getSecureDomains()
 * @method Scrotis recordDataInDashboardFeed() @payload [DataElementName, DateTimeAdded, DashboardData]
 * @method Scrotis select($table)
 * @method Scrotis updates($table)
 * @method Scrotis updateDisplayedProducerBanner() @payload [dateTime, ProducerID, CategoryID]
 * @method Scrotis insertDisplayedProducerBanner()
 * @method Scrotis insertIntoProducerBanner() @payload [ProducerID, CategoryID, DisplayTime, BannerPath, Position]
 * @method Scrotis deleteOldProducerBannerRows($category_id)
 * @method Scrotis getDisplayTimes()
 * @method Scrotis getAllRecentlyUpdatedStudiosForEachCategory($category_id)
 * @method Scrotis insertIntoProducersWithoutBannerFile() @payload [ProducerID]
 * @method Scrotis getCheckRequest()
 * @method Scrotis updateMarkAsStarted()
 * @method Scrotis updateMarkAsComplete()
 * @method Scrotis getPromotions()
 * @method Scrotis deleteCleanupBannersQueue()
 * @method Scrotis insertPromotionBannersQueue() @payload [PromotionBannerID, DestinationURL, ImageURL, DisplayTime, Position, Location]
 * @method Scrotis getDate()
 * @method Scrotis getDownloadedFileChunks()
 * @method Scrotis getActiveProducerClipMembers($ClipMemberID)
 * @method Scrotis getActiveImageMembers($ClipMemberID)
 * @method Scrotis deleteNotActiveChunk($ClipMemberID)
 * @method Scrotis createTrafficTableByDay() @payload [day]
 * @method Scrotis customStoreNameIDExists()
 * @method Scrotis statsIdsForDate() @payload [ids, date]
 * @method Scrotis getClipsToBeActivated($clip_id = null)
 * @method Scrotis setClipActive($clip_id)
 * @method Scrotis setClipDateForStoreProducerClips($clip_id)
 * @method Scrotis setClipUpdatedForProducerClipSite($producer_id)
 * @method Scrotis getUpdated($producer_id)
 * @method Scrotis insertHash() @payload [...data]
 * @method Scrotis getProducerClipSiteBy($producer_id, $param = []) @param [catid]
 * @method Scrotis getClipExchanges() @param [date]
 * @method Scrotis bulkStudioTrafficStats() @param [ids]
 * @method Scrotis updateStatisticalTotals() @payload [StatisticalTotalID, ProducerID, TotalSales, TotalProducerCommission, TotalHitsPerDay, DataDate]
 * @method Scrotis getOrdersNotDownloaded()
 * @method Scrotis updateOrdersByProducerID($producer_id)
 * @method Scrotis getVideoStoresToClose()
 * @method Scrotis getImageStoresToClose()
 * @method Scrotis scan()
 * @method Scrotis saveTweetResponse() @payload [Class, Response, ResponseStatus, TweetID, ProducerID, Tweet, CreateAt]
 * @method Scrotis getAllScheduleLogs() @param [counts, class, tweet_id, producer_id, status, per_page, page, sort_field, sort_order]
 * @method Scrotis deleteScheduleLogsBy($id)
 * @method Scrotis deletestatisticaltotalsby() @param [date]
 * @method Scrotis lastStudioSignUpsByDate()
 * @method Scrotis getSentEmailsInfo()
 * @method Scrotis getChangeLogClipsBy() @param [active, minute]
 * @method Scrotis getUsersDatabyid()
 * @method Scrotis getUserEmailWithForeverSessionId()
 **/
class Utilities extends Scrotis
{

}

/**
 * @package Exchange
 * @method Scrotis sonumberByClipID($clip_id)
 * @method Scrotis activateAllClipMembers() @payload [so_number, user]
 * @method Scrotis activateAllImageMembers() @payload [so_number, user]
 * @method Scrotis activateMediaType() @payload [type, so_number, file_name, media_id, user]
 * @method Scrotis getProducerMembers() @param [media_type, so_number]
 * @method Scrotis studioIdBySoNumber($so_number) @param [type]
 * @method Scrotis mediaByProducerId($producer_id) @param [page, per_page, media_type, keyword]
 * @method Scrotis executeExchangeUpdate($producer_id) @payload [so_number, old_item, new_item, media_type, username, comment]
 * @method Scrotis exchangeAndActivationLog() @param [so_number]
 * @method Scrotis activateSelectedSonumbers() @payload [user, reason, so_numbers]
 * @method Scrotis deactivateSelectedSonumbers() @payload [user, reason, so_numbers]
 **/
class Exchange extends Scrotis
{

}

/**
 * @package Clipcashaccounts
 * @method Scrotis clipCashAccounts() @param [email]
 **/
class Clipcashaccounts extends Scrotis
{

}


/**
 * @package Accounttype
 * @method Scrotis show()
 * @method Scrotis type($account_id)
 **/
class Accounttype extends Scrotis
{

}

/**
 * @package Affiliate
 * @method Scrotis counter($id)
 * @method Scrotis show()
 * @method Scrotis getCid() @payload [producerid, contest_id]
 * @method Scrotis getHits()
 * @method Scrotis getTransactions()
 * @method Scrotis getSaleSum()
 * @method Scrotis getContestRanking()
 * @method Scrotis getContestsRanking()
 * @method Scrotis getContestWinners()
 * @method Scrotis ProducerPercentageOverride()
 * @method Scrotis getProducerPercentage()
 * @method Scrotis contestDetails($contest_id)
 * @method Scrotis discountCid($id)
 * @method Scrotis incentiveHits() @param [cid, contestid, type]
 * @method Scrotis getByCid() @param [cid]
 * @method Scrotis getTransactionsBy() @param [cid]
 **/
class Affiliate extends Scrotis
{

}

/**
 * @package Animatedgifcapturetimes
 * @method Scrotis show($clip_id = null)
 * @method Scrotis getId($clip_id)
 * @method Scrotis clipExists($clip_id)
 * @method Scrotis getMaxSortOrder($clip_id)
 **/
class Animatedgifcapturetimes extends Scrotis
{

}


/**
 * @package Auth
 * @method Scrotis validateToken() @payload [access_token]
 * @method Scrotis refreshToken() @payload [refresh_token]
 * @method Scrotis extendToken() @payload [access_token]
 * @method Scrotis getUser() @payload [access_token]
 * @method Scrotis signin() @payload [Email, Password]
 * @method Scrotis checkUsernameAvailability() @payload [Username]
 * @method Scrotis checkEmailAvailability() @payload [Email]
 * @method Scrotis checkActivationCode() @payload [Code]
 * @method Scrotis passwordResetRequest() @payload [Email]
 * @method Scrotis resetPassword() @payload [Access_token, Activation_Code, Password]
 * @method Scrotis updateProfile() @payload [access_token, username, first_name, last_name, phone_number, country, state]
 * @method Scrotis updatePassword() @payload [password, access_token]
 * @method Scrotis registerUser() @payload [Email]
 **/
class Auth extends Scrotis
{

}

/**
 * @package Badstores
 * @method Scrotis show()
 * @method Scrotis getBadStoresAndProducers()
 * @method Scrotis addBadProducer()
 * @method Scrotis removeBadProducer()
 * @method Scrotis addBadStore()
 * @method Scrotis closedStores()
 * @method Scrotis reopenClosedStore()
 * @method Scrotis exists($producer_id)
 * @method Scrotis badStoreReason($pid)
 * @method Scrotis badStoresChangeLog() @param [studio_id, from, to, page, per_page]
 **/
class Badstores extends Scrotis
{

}

/**
 * @package Badwords
 * @method Scrotis show()
 * @method Scrotis getAllBadWords()
 * @method Scrotis createBadWord()
 * @method Scrotis deleteBadWord($id)
 * @method Scrotis cleanStringFromBadWords() @param [str]
 **/
class Badwords extends Scrotis
{

}

/**
 * @package Banners
 * @method Scrotis promotionBanners()
 * @method Scrotis bannerInfo($producer_id)
 * @method Scrotis categoryCount($category_id)
 * @method Scrotis category($category_id)
 * @method Scrotis nextRotation($category_id)
 * @method Scrotis topBannersCarousel($id)
 * @method Scrotis getBannersForDataTable()
 * @method Scrotis fillExpiredBanners($category_id)
 **/
class Banners extends Scrotis
{

}

/**
 * @package BannedBadWords
 * @method Scrotis show()
 * @method Scrotis delete($id)
 * @method Scrotis edit($id)
 * @method Scrotis create() @payload [BannedWord, AltWord]
 **/
class BannedBadWords extends Scrotis
{

}


/**
 * @package Batchemail
 * @method Scrotis mailServers()
 * @method Scrotis pendingEmails()
 * @method Scrotis emailsToSendout()
 * @method Scrotis setStartTime($id) @payload [BatchEmail_messageID]
 * @method Scrotis setEndTime($id) @payload [BatchEmail_messageID]
 * @method Scrotis markRecipientAsCompleted() @param [message_id, user_id, exim]
 * @method Scrotis markRecipientAsCompletedBulk() @payload [message_id, user_id, exim]
 * @method Scrotis messageRecipients($id)
 * @method Scrotis blockedDomains()
 * @method Scrotis denyEmail() @payload [PendingEmailID, ProducerID, reason, support_user]
 * @method Scrotis batchEmailMessagesHistory() @payload [message_id, created_at, sent, total]
 **/
class Batchemail extends Scrotis
{

}


/**
 * @package Bundle
 * @method Scrotis bundle($BundleID)
 * @method Scrotis bundleClips($BundleID)
 * @method Scrotis delete($BundleID)
 * @method Scrotis getProducersBundleClips() @param [bundle_ids]
 * @method Scrotis cartBundle() @param [bundle_ids]
 **/
class Bundle extends Scrotis
{

}

/**
 * @package C4slive
 * @method Scrotis show()
 * @method Scrotis domain($producer_id)
 * @method Scrotis stores($producer_id)
 * @method Scrotis validated($producer_id)
 * @method Scrotis galleryID()
 * @method Scrotis getIDbyTitle($producer_id)
 * @method Scrotis gallery($gallery_id)
 * @method Scrotis galleryWhere($producer_id)
 * @method Scrotis galleryWithCatsWhere($producer_id)
 * @method Scrotis countGalleryWhere($producer_id)
 * @method Scrotis storesOnline($producer_id = null)
 * @method Scrotis userInfo($id)
 * @method Scrotis storeSite($id)
 * @method Scrotis badName()
 * @method Scrotis domainAvailable($producer_id)
 * @method Scrotis storeSitePages($id)
 * @method Scrotis getWebCam($producer_id)
 * @method Scrotis webcam($id)
 * @method Scrotis articles($id)
 * @method Scrotis articlesWhere($producer_id)
 * @method Scrotis countArticlesWhere($producer_id)
 * @method Scrotis calendar($id)
 * @method Scrotis calendarWhere($producer_id)
 * @method Scrotis countCalendarWhere($producer_id)
 * @method Scrotis gallerySetImage($gallery_no)
 * @method Scrotis galleryCats()
 * @method Scrotis countryName()
 * @method Scrotis updateGallery($id) @param [fkid]
 * @method Scrotis getGallery($id) @param [title]
 * @method Scrotis subscriptions($store_id)
 * @method Scrotis subscriptionsByMemberID($member_id)
 * @method Scrotis getStoresByDomain() @param [domain]
 **/
class C4slive extends Scrotis
{

}

/**
 * @package Cache
 * @method Scrotis studioBan($studio_id)
 * @method Scrotis categoryBan($studio_id)
 **/
class Cache extends Scrotis
{

}

/**
 * @package Captcha
 * @method Scrotis show()
 * @method Scrotis getbyip()
 * @method Scrotis insert()
 **/
class Captcha extends Scrotis
{

}

/**
 * @package Category
 * @method Scrotis carousel()
 * @method Scrotis catsQueue()
 * @method Scrotis getCategories()
 * @method Scrotis getCategoryName($id)
 * @method Scrotis redisCategories()
 * @method Scrotis redisCategoryCounts()
 * @method Scrotis redisCategoryStoreCounts($category_id)
 * @method Scrotis redisRelatedCategories($category_id)
 * @method Scrotis createCategory()
 * @method Scrotis catsQueueNew()
 * @method Scrotis catsQueueNewStores()
 * @method Scrotis clipcounts()
 * @method Scrotis trendingCategoriesWithCounts()
 * @method Scrotis recentClipsByCategories()
 * @method Scrotis fetishCategories() @param [category_id, category, rank, added_by]
 **/
class Category extends Scrotis
{

}

/**
 * @package Cart
 * @method Scrotis cart()
 * @method Scrotis items() @param [session_id]
 * @method Scrotis addToCart() @payload [session_id, producer_id, item_id, item_type]
 * @method Scrotis removeFromCart() @payload [session_id, item_id, item_type, producer_id]
 * @method Scrotis metadata() @param [session_id]
 * @method Scrotis availableStatuses()
 * @method Scrotis redis() @param [session_id, transform]
 * @method Scrotis cartViewed()
 * @method Scrotis clear()
 * @method Scrotis set() @payload [session_id, cart_items]
 * @method Scrotis add() @payload [session_id, items]
 * @method Scrotis remove() @payload [session_id, items]
 * @method Scrotis abandonedCartData() @param [user_type, from, to, page, per_page]
 * @method Scrotis setPurchasedStatus()
 * @method Scrotis abandonedCartByDatePrecision()
 * @method Scrotis markAbandonedCarts() @payload [hours]
 **/
class Cart extends Scrotis
{

}

/**
 * @package Clipformats
 * @method Scrotis getExtensions()
 * @method Scrotis getHotelExtensions()
 * @method Scrotis mimeType()
 **/
class Clipformats extends Scrotis
{

}

/**
 * @package Clippreviewcapturetimes
 * @method Scrotis show()
 * @method Scrotis getCaptureTime($clip_id)
 **/
class Clippreviewcapturetimes extends Scrotis
{

}

/**
 * @package Clippreviewcapturetimesv2()
 * @method Scrotis show($clip_id = null)
 * @method Scrotis getCaptureTime($clip_id)
 **/
class Clippreviewcapturetimesv2 extends Scrotis
{

}

/**
 * @package Clipreview
 * @method Scrotis requestClipReview() @payload [producer_id, clip_id, user, clip_name, user_ip]
 **/
class Clipreview extends Scrotis
{

}

/**
 * @package Clips
 * @method Scrotis show($clip_id)
 * @method Scrotis elastic($clip_id)
 * @method Scrotis getProducerNameImage($producer_id)
 * @method Scrotis getClipByName($producer_id)
 * @method Scrotis getClipByTitle($producer_id) @payload  [title]
 * @method Scrotis getClipByTitleAndStudio($producer_id) @payload [ClipTitle]
 * @method Scrotis getActiveClipsByTitle($producer_id)
 * @method Scrotis getCategoryName($clip_id)
 * @method Scrotis getCategoryFromRedis($clip_id)
 * @method Scrotis updateClipPreviews($producer_id)
 * @method Scrotis updatePreviewMethod($clip_id)
 * @method Scrotis deleteClip($producer_id) @payload [ClipID]
 * @method Scrotis addLog($producer_id)
 * @method Scrotis updateAddLogWhereDeleteDTTMisNull($producer_id) @payload [ClipID, Delete_dttm]
 * @method Scrotis getLastGoodUpdateTime($producer_id)
 * @method Scrotis getClipNameImage($clip_id)
 * @method Scrotis getClipCat($producer_id)
 * @method Scrotis getClipActive($producer_id)
 * @method Scrotis getLatestClipID($producer_id) @payload [clipcat]
 * @method Scrotis getPricing()
 * @method Scrotis minPrice($minutes)
 * @method Scrotis futureActive($producer_id)
 * @method Scrotis getAll($clip_id)
 * @method Scrotis randomHalfPriceClip($producer_id)
 * @method Scrotis nextClips($clip_id)
 * @method Scrotis elasticGetClips() @payload [clip_ids]
 * @method Scrotis redisGetClips() @payload [clip_ids]
 * @method Scrotis redisTopClips() @payload [category_id]
 * @method Scrotis elasticTopClips() @payload [category_id, page, per_page, days, startdate, enddate]
 * @method Scrotis redisTopClipswithStudio() @payload [category_id]
 * @method Scrotis details($clip_id)
 * @method Scrotis metadata($id)
 * @method Scrotis rfsMeta($clip_id)
 * @method Scrotis previewFormat()
 * @method Scrotis filesize($clip_id)
 * @method Scrotis reindex($clip_id)
 * @method Scrotis mostRecentClipsSold()
 * @method Scrotis category($cat_id)
 * @method Scrotis categoryCount($cat_id)
 * @method Scrotis tagged($cat_id)
 * @method Scrotis mobileClipSearchES($category_id) @payload [keyword, catid, startlimit, endlimit, sortby]
 * @method Scrotis mobileRecentlyAddedClips() @payload [categoryId, keyword, page, limit, fromDate]
 * @method Scrotis hasCustomPreview($clip_id)
 * @method Scrotis customPreview($clip_id)
 * @method Scrotis recognizer($memberID)
 * @method Scrotis batchImport($batch_id = null)
 * @method Scrotis checkClipContent()
 * @method Scrotis lastActivated($minutes)
 * @method Scrotis previewType()
 * @method Scrotis related($clip_id) @payload [limit]
 * @method Scrotis canDelete($producer_id) @payload [ClipID, ClipName]
 * @method Scrotis images($clip_id)
 * @method Scrotis getClipsReports() @payload [filter]
 * @method Scrotis getClipsChartData() @payload [filter, from, to]
 * @method Scrotis pendingPreviews() @payload [ClipDate]
 * @method Scrotis changeloggerFirstRows($clip_id)
 * @method Scrotis changeloggerLastRows($clip_id)
 * @method Scrotis fromChangeLogger($clip_id)
 * @method Scrotis producerClipsData() @payload [producer_id, page, limit, days]
 * @method Scrotis deletedClips($producer_id) @payload [weeks, per_page, page, sort_field, sort_order]
 * @method Scrotis deletedClipsDetails($producer_id)
 * @method Scrotis clipInfo($producer_id) @param [generated, cliptitle, clipname, clipimage]
 * @method Scrotis latestRowHash($producer_id) @param [clipcat]
 * @method Scrotis producerInfo($clip_id)
 * @method Scrotis searchClipsByField() @param [column, value]
 * @method Scrotis getClipsByRequest() @param [column, value]
 * @method Scrotis getActiveClipsCount($producer_id)
 * @method Scrotis clips($producer_id) @param [fields, ids]
 * @method Scrotis latestClips($producer_id)
 * @method Scrotis topCategories($producer_id)
 * @method Scrotis mostRecentUpdates($producer_id) @param [limit]
 * @method Scrotis hasCorruptClips($producer_id)
 * @method Scrotis getStudioClipsForSupportTools() @param [studio_id, page, per_page, keyword, order, sort, search_all]
 * @method Scrotis allActiveClips() @param [from to page size sort order]
 * @method Scrotis bestFetishClips() @param [keyword days page per_page]
 * @method Scrotis relatedTopClips() @param [category_id page per_page days startdate enddate streaming_page]
 * @method Scrotis getPerformersByClipId($id)
 * @method Scrotis getCountClipsWithPerformersByProducerID($id) @param [studio_id]
 * @method Scrotis getNumberOfClipsSetForActivationByProducerId() @param [producer_id]
 * @method Scrotis topSellingClipByCategories()
 * @method Scrotis exploreYourFetishClips()
 * @method Scrotis webm()
 * @method Scrotis mp4()
 * @method Scrotis mp4Webm()
 * @method Scrotis complianceclIpInfo()
 **/
class Clips extends Scrotis
{

}

/**
 * @package Clipservicesconf
 * @method Scrotis getAllPendingToEncode($studio_id) @param [fields, page, per_page]
 * @method Scrotis byClipIds($studio_id) @param [clip_ids, attribute]
 * @method Scrotis notEncoded() @param [page, perPage]
 **/
class Clipservicesconf extends Scrotis
{

}

/**
 * @package Clipsiteservicesconf
 * @method Scrotis all()
 * @method Scrotis show($producer_id)
 * @method Scrotis getValueCount() @payload [key, frequency, hour]
 * @method Scrotis getNumberOfStudiosWhoEnablesAutoTweets() @payload [key, frequency, hour]
 **/
class Clipsiteservicesconf extends Scrotis
{

}

/**
 * @package Cms
 * @method Scrotis show($id)
 * @method Scrotis post($id)
 * @method Scrotis page($id)
 * @method Scrotis categoryPosts($category_id)
 * @method Scrotis categories()
 * @method Scrotis category($category_id)
 **/
class Cms extends Scrotis
{

}

/**
 * @package Contentmanagement
 * @method Scrotis getSettings()
 * @method Scrotis getSettingName() @payload [name]
 * @method Scrotis getPageDataForCategory($category_id)
 * @method Scrotis getPageData($page_name)
 * @method Scrotis getNumDaysOutgoingLinksDisabled()
 * @method Scrotis activeWebServers()
 * @method Scrotis newFeaturesNews($id = null)
 * @method Scrotis topBannerHeader()
 * @method Scrotis contentremoval()
 **/
class Contentmanagement extends Scrotis
{

}

/**
 * @package Countries
 * @method Scrotis show()
 * @method Scrotis getBannedCountries()
 * @method Scrotis countryList()
 * @method Scrotis getAllCountries()
 * @method Scrotis editScrutinize()
 * @method Scrotis countriesWithFilters()
 * @method Scrotis countryCurrency()
 * @method Scrotis countryCurrencies()
 **/
class Countries extends Scrotis
{

}

/**
 * @package Customer
 * @method Scrotis getUserClips($clip_id)
 * @method Scrotis details($clip_id)
 * @method Scrotis testClips()
 * @method Scrotis insertClipPurchaseHistory() @payload [so_number, user_id]
 * @method Scrotis getCustomerPurchases($customer_id) @param [page, per_page, id]
 * @method Scrotis customers() @param [name, email, country, city, state, zip, address_one, address_two, page, size]
 * @method Scrotis accountOrders() @param [email, page, size]
 * @method Scrotis customer()
 * @method Scrotis blockedUsersDueToChargeback()
 **/
class Customer extends Scrotis
{

}

/**
 * @package Dashboard
 * @method Scrotis clips($user_id) @payload [page, limit]
 * @method Scrotis wishlist($user_id) @payload [page, limit]
 * @method Scrotis wishlistForStudioPage($user_id) @payload [producer_id]
 * @method Scrotis addWishlist() @payload [user_id, clip_id, producer_id]
 * @method Scrotis removeWishlist() @payload [user_id, clip_id]
 * @method Scrotis getCart($user_id) @payload [user_id]
 * @method Scrotis setCart() @payload [user_id, cart]
 * @method Scrotis removeCart() @payload [user_id]
 * @method Scrotis associateUserEmail() @payload [user_id, email, host]
 * @method Scrotis importOrdersRequest() @payload [user_id, email]
 * @method Scrotis verifyEmail() @payload [activation_code, access_token]
 * @method Scrotis associatedEmails($user_id) @param [page, per_page]
 **/
class Dashboard extends Scrotis
{

}

/**
 * @package Details
 * @method Scrotis producerDetails($producer_id)
 * @method Scrotis reIndex($detail_id)
 **/
class Details extends Scrotis
{

}

/**
 * @package Dmca
 * @method Scrotis filingRecord($id) @payload [ProducerID, ComplainantName, ComplainantEmail, dmcaDate, counterDate, dmcaStatus, Casenumber, TicketNumber, site, Staff]
 * @method Scrotis filingRecordAll() @param [search, page, per_page]
 * @method Scrotis getRecord() @param [producer_id, complainant_name, complainant_email, dmca_date]
 * @method Scrotis status($status)
 * @method Scrotis dmcaId($dmca_id)
 * @method Scrotis countComplaints() @param [email]
 * @method Scrotis agentRemovedEmail()
 * @method Scrotis originatorRemovedEmail()
 * @method Scrotis studioRemovedEmail()
 * @method Scrotis studioCounterEmail()
 * @method Scrotis originatorCounterEmail()
 * @method Scrotis studioCounterReceivedEmail()
 **/
class Dmca extends Scrotis
{

}

/**
 * @package Domains
 * @method Scrotis isRedirectAllowed() @payload [Domain]
 **/
class Domains extends Scrotis
{

}

/**
 * @package Download
 * @method Scrotis clip($clipmemberid)
 * @method Scrotis clipActive($order_id)
 * @method Scrotis clipID($order_id) @param [clipid]
 * @method Scrotis image($image_member_id)
 * @method Scrotis imageActive($order_id)
 * @method Scrotis country($iplong)
 * @method Scrotis trackOrder($SONumber)
 * @method Scrotis ndb()
 * @method Scrotis failed($SONumber) @param [clipid] @payload [SONumber, ClipID, FilePath, ProducerContacted, Resolved, DateCreated, DateResolved]
 * @method Scrotis failedDownloads($id = null)
 * @method Scrotis notifiedDownloads($id = null)
 * @method Scrotis resolvedDownloads()
 * @method Scrotis disableClip($producer_id) @payload [clipid]
 * @method Scrotis decrementClipDownload($ClipMemberID)
 * @method Scrotis decrementImageDownload($ImageMemberID)
 * @method Scrotis fileChunks($id)
 * @method Scrotis clipDownloads()
 * @method Scrotis generateLink()
 * @method Scrotis getClipDownloads() @param [page, size, search]
 * @method Scrotis stats() @param [DC, ClipMemberID, per_page, page, sort_field, sort_order]
 **/
class Download extends Scrotis
{

}

/**
 * @package Email
 * @method Scrotis show($message_id)
 * @method Scrotis send() @payload [fromName, fromEmail, to, subject, body, contentType, headers, mailer]
 * @method Scrotis activeSubscribersbyEmail($producer_id) @payload [Email]
 * @method Scrotis unsubscribe($producer_id)
 * @method Scrotis totalEmailsforProducer($producer_id)
 * @method Scrotis pendingEmail()
 * @method Scrotis messageBody($message_id)
 * @method Scrotis batchEmailMessage()
 * @method Scrotis totalRecipients($message_id)
 * @method Scrotis totalPendingRecipients($message_id)
 * @method Scrotis inactive($producer_id)
 * @method Scrotis count($producer_id)
 * @method Scrotis emails($producer_id) @payload [search, order, sort, limit, offset]
 * @method Scrotis scrub() @payload [text]
 * @method Scrotis internal() @payload [to, from, subject, message, mail_type]
 * @method Scrotis notify() @payload [type, subject, message, mail_type]
 * @method Scrotis dmca() @payload [to, subject, body, fromEmail, contentType]
 * @method Scrotis track()
 * @method Scrotis globalUnsubscribe()
 * @method Scrotis getGlobalSubscriptions() @payload [page, limit, sortby, order, email]
 * @method Scrotis addProducer()
 * @method Scrotis addBatchEmail()
 * @method Scrotis addRecipient()
 * @method Scrotis getTotals() @param [start, end]
 * @method Scrotis updateEmailSubscription($id)
 * @method Scrotis subscribeGlobal() @payload [email]
 **/
class Email extends Scrotis
{

}

/**
 * @package Faq
 * @method Scrotis categories()
 * @method Scrotis articles()
 **/
class Faq extends Scrotis
{

}

/**
 * @package Feeds
 * @method Scrotis show()
 * @method Scrotis recentlyUpdatedStudios()
 * @method Scrotis recentlyUpdatedClips()
 * @method Scrotis topclips()
 **/
class Feeds extends Scrotis
{

}

/**
 * @package Ftp
 * @method Scrotis authenticate()
 * @method Scrotis allowedExtensions()
 * @method Scrotis extensionType()
 * @method Scrotis logUpload()
 * @method Scrotis uploadLog()
 * @method Scrotis violation()
 * @method Scrotis extensions()
 * @method Scrotis types()
 * @method Scrotis newChunk()
 **/
class Ftp extends Scrotis
{

}

/**
 * @package Futureactivationqueue
 * @method Scrotis show()
 * @method Scrotis getID()
 **/
class Futureactivationqueue extends Scrotis
{

}

/**
 * @package Geo
 * @method Scrotis countryCodefromIP()
 * @method Scrotis countries()
 * @method Scrotis subdivisions()
 * @method Scrotis cities()
 **/
class Geo extends Scrotis
{

}

/**
 * @package Geoip
 * @method Scrotis isp($ip_address)
 * @method Scrotis show($ip_address)
 * @method Scrotis isAws()
 **/
class Geoip extends Scrotis
{

}

/**
 * @package Health
 * @method Scrotis version()
 * @method Scrotis ping()
 * @method Scrotis configHosts()
 * @method Scrotis show()
 **/
class Health extends Scrotis
{

}

/**
 * @package Hermes
 * @method Scrotis show()
 * @method Scrotis queue($id = null) @payload [clip_id, producer_id, in_file, out_file, time_added]
 **/
class Hermes extends Scrotis
{

}

/**
 * @package Hiddenproducers
 * @method Scrotis show()
 * @method Scrotis hiddenProducersWithReason()
 * @method Scrotis producer()
 * @method Scrotis history()
 **/
class Hiddenproducers extends Scrotis
{

}

/**
 * @package Highwinds
 * @method Scrotis purge() @payload [path, recursive, invalidate]
 **/
class Highwinds extends Scrotis
{

}

/**
 * @package I4s
 * @method Scrotis show()
 * @method Scrotis recentlyUpdated()
 * @method Scrotis new()
 * @method Scrotis searchImageStores()
 * @method Scrotis topStores()
 * @method Scrotis topSellingImages()
 * @method Scrotis moreGreatStores()
 * @method Scrotis featuredStoreBanner()
 * @method Scrotis checkOrder()
 * @method Scrotis getTitleById()
 **/
class I4s extends Scrotis
{

}

/**
 * @package I4sdataupdate
 * @method Scrotis nonActiveImages()
 * @method Scrotis imageCatForUpdatedImageStore()
 * @method Scrotis topImageStoresData()
 * @method Scrotis topImagesData()
 * @method Scrotis newImageStoresData()
 * @method Scrotis openImageStoresData()
 * @method Scrotis updatedImageStoresData()
 **/
class I4sdataupdate extends Scrotis
{

}

/**
 * @package Imageformats
 * @method Scrotis getExtensions()
 **/
class Imageformats extends Scrotis
{

}

/**
 * @package Images
 * @method Scrotis show()
 * @method Scrotis canDelete()
 * @method Scrotis details()
 * @method Scrotis getTopSellingImages()
 * @method Scrotis getAllImagesList()
 * @method Scrotis getStoreImages()
 * @method Scrotis searchImages()
 * @method Scrotis getStoreData()
 * @method Scrotis getImageData()
 * @method Scrotis getAllImageStores()
 * @method Scrotis getListZipContents()
 * @method Scrotis getImageZip($producer_id)
 * @method Scrotis getImageIDs() @param [ImageIDs]
 * @method Scrotis image($producer_id)
 * @method Scrotis images($producer_id) @param [fields]
 * @method Scrotis storeProducerImages($producer_id) @param [search, orderby, sort, status, data1, data2, page, per_page]
 * @method Scrotis imageChanging($image_id)
 * @method Scrotis countImageSetsByImageHash($producer_id) @param [imagehash]
 * @method Scrotis countImageSetsByZipName($producer_id) @param [imagezip]
 * @method Scrotis imageByTitle($producer_id) @param [ImageTitle]
 * @method Scrotis imageIDByZip($producer_id) @param [zip]
 * @method Scrotis getActiveStatus($image_id) @param [zip]
 **/
class Images extends Scrotis
{

}

/**
 * @package ImageHash
 * @method Scrotis imageFirstActiveDate($image_id) @param [ImageHash]
 * @method Scrotis producerImageHash()
 * @method Scrotis imageHashExists() @param [hash]
 * @method Scrotis updateByImageHash() @payload [ImageID, ImageHash]
 **/
class ImageHash extends Scrotis
{

}

/**
 * @package Keywords
 * @method Scrotis summaries()
 * @method Scrotis insertLogKeywords() @payload [search_phrase, type, count, created_at, updated_at]
 * @method Scrotis topDailyKeywords() @param [limit] @payload [search_phrase, type, count, created_at, updated_at]
 * @method Scrotis topKeywordsByFilters() @param [type, from, to, sort, order, page, size]
 * @method Scrotis clearTopKeywords()
 **/
class Keywords extends Scrotis
{

}

/**
 * @package Linkchecker
 * @method Scrotis show()
 * @method Scrotis deleteSite()
 * @method Scrotis checkLinks()
 * @method Scrotis nonReciprocatingLinkOverrideData()
 * @method Scrotis insertNonReciprocatingLinkOverrideRecord()
 * @method Scrotis deleteNonReciprocatingLinkOverrideRecord()
 * @method Scrotis getProhibitedDomains()
 * @method Scrotis insertProhibitedDomain()
 * @method Scrotis deleteProhibitedDomain()
 * @method Scrotis getPermanentlyDeniedRedirectToDomain()
 * @method Scrotis insertIntoPermanentlyDeniedRedirectToDomain()
 * @method Scrotis updateInPermanentlyDeniedRedirectToDomain()
 * @method Scrotis deleteFromPermanentlyDeniedRedirectToDomain()
 * @method Scrotis getTrackLinks()
 **/
class Linkchecker extends Scrotis
{

}

/**
 * @package Listpage
 * @method Scrotis recentlyUpdatedStudio($producer_id = null, $param = []) @param [CategoryID]
 * @method Scrotis getId()
 * @method Scrotis getCount()
 * @method Scrotis deleteLimitCategory($noid, $param = []) @param [CategoryID, limit]
 **/
class Listpage extends Scrotis
{

}

/**
 * @package Merchant
 * @method Scrotis cc()
 * @method Scrotis address()
 * @method Scrotis allSites()
 * @method Scrotis merchantList()
 * @method Scrotis merchantAccountList()
 **/
class Merchant extends Scrotis
{

}

/**
 * @package Messages
 * @method Scrotis lastMessages()
 * @method Scrotis create()
 * @method Scrotis update()
 * @method Scrotis delete()
 **/
class Messages extends Scrotis
{

}

/**
 * @package Migrations
 * @method Scrotis show()
 * @method Scrotis dmcaId()
 * @method Scrotis pendingRemoval()
 * @method Scrotis pendingRestore()
 * @method Scrotis search()
 * @method Scrotis getByProducerID()
 **/
class Migrations extends Scrotis
{

}

/**
 * @package Monitoringsystem
 * @method Scrotis show()
 * @method Scrotis delete()
 * @method Scrotis preview()
 * @method Scrotis cache()
 * @method Scrotis jpglog()
 * @method Scrotis cacheClear()
 **/
class Monitoringsystem extends Scrotis
{

}

/**
 * @package ModelRelease
 * @method Scrotis modelRelease($producer_id) @param [FileName, passKey, ModelReleaseID]
 * @method Scrotis modelReleaseExists($producer_id) @param [FileName]
 * @method Scrotis updateModelRelease($producer_id)
 * @method Scrotis insertModelRelease($producer_id)
 **/
class ModelRelease extends Scrotis
{

}

/**
 * @package ModelID
 * @method Scrotis modelID($producer_id) @param [FileName, passKey, ModelIdentID]
 * @method Scrotis modelIDExists($producer_id) @param [FileName]
 * @method Scrotis updateModelID($producer_id)
 * @method Scrotis insertModelID($producer_id)
 **/
class ModelID extends Scrotis
{

}

/**
 * @package Noncustomer
 * @method Scrotis processingcurrency() @payload [ip]
 **/
class Noncustomer extends Scrotis
{

}

/**
 * @package Oauthtokens
 * @method Scrotis id()
 * @method Scrotis getActiveToken()
 * @method Scrotis getActiveTokenID()
 * @method Scrotis twitter()
 **/
class Oauthtokens extends Scrotis
{

}

/**
 * @package Owners
 * @method Scrotis show()
 * @method Scrotis update()
 * @method Scrotis producerIds()
 * @method Scrotis producer_ids()
 * @method Scrotis linkProducer()
 * @method Scrotis link_producer()
 * @method Scrotis ownerIDbyLogin()
 * @method Scrotis ownerProducer()
 * @method Scrotis removeProducer()
 * @method Scrotis storeStatus()
 **/
class Owners extends Scrotis
{

}

/**
 * @package Passwordtoken
 * @method Scrotis show()
 * @method Scrotis producer()
 * @method Scrotis token()
 * @method Scrotis producerTokenExists()
 * @method Scrotis assignToken()
 **/
class Passwordtoken extends Scrotis
{

}

/**
 * @package Payoneeraccounts
 * @method Scrotis show()
 * @method Scrotis payoneerSignupUrl()
 * @method Scrotis addProducer()
 * @method Scrotis removeProducer()
 **/
class Payoneeraccounts extends Scrotis
{

}

/**
 * @package Payouts
 * @method Scrotis payouts()
 **/
class Payouts extends Scrotis
{

}

/**
 * @package Permissions
 * @method Scrotis canDownload()
 * @method Scrotis producerFileManagerUser()
 * @method Scrotis ftpTempUser()
 **/
class Permissions extends Scrotis
{

}

/**
 * @package Ping
 * @method Scrotis show()
 **/
class Ping extends Scrotis
{

}

/**
 * @package Producercats
 * @method Scrotis show($id = null)
 * @method Scrotis getCategory($cat_id)
 * @method Scrotis getProducerIdforCategoryId($cat_id) @payload [producer_id]
 * @method Scrotis categoryData() @param [cat_id, category, ids, exclude_ids, page, size] @payload [category, g, cat_id]
 * @method Scrotis categoryMappingData() @param [mapping_id, parent_id, child_id] @payload [mapping_id, parent_id, child_id]
 **/
class Producercats extends Scrotis
{

}

/**
 * @package Producerclipcats
 * @method Scrotis show($id)
 * @method Scrotis getProducerIdforCategoryId($cat_id) @payload [producer_id]
 * @method Scrotis decrementClipCats($producer_id)
 * @method Scrotis getClipCount($producer_id) @payload [catid]
 * @method Scrotis clipCategories($producer_id)
 * @method Scrotis updateByLatestClipID($LatestClipID)
 * @method Scrotis updateClipCount() @payload [clip_count, producer_id, cat_id]
 * @method Scrotis updateLatestClipID() @payload [ProducerID, LatestClipID, cat_id]
 * @method Scrotis deleteOutdated($id)
 **/
class Producerclipcats extends Scrotis
{

}

/**
 * @package Producercliphash
 * @method Scrotis show()
 * @method Scrotis getByClipID()
 * @method Scrotis clipID()
 * @method Scrotis getClipID()
 * @method Scrotis getCountByHash($clip_id, $param = []) @param [ClipHash]
 * @method Scrotis getFirstActive()
 **/
class Producercliphash extends Scrotis
{

}

/**
 * @package Producerclips
 * @method Scrotis show()
 * @method Scrotis getTotalClips($producer_id)
 * @method Scrotis getClips($producer_id)
 * @method Scrotis getClipCategories()
 * @method Scrotis replaceClipName()
 * @method Scrotis updateClips($producer_id)
 * @method Scrotis corruptedClipsLog($producer_id) @param [per_page, page, keyword, sort_field, sort_order]
 * @method Scrotis clipsinfo($producer_id)
 * @method Scrotis insertProducerClipMember() @payload [producer_id, clip_name, ip_clip_id, so_number, drm_active]
 * @method Scrotis getClipCat($id)
 * @method Scrotis clipMembers() @param [so_number, clip_id]
 * @method Scrotis getCountStoreProducerClipsByProducerId($store_id) @param [so_number, clip_id]
 * @method Scrotis clipsByKeyword() @param [search, from, to, page, size]
 * @method Scrotis totalByKeyword() @param [search, from, to]
 * @method Scrotis getClipsCountByActiveStatus($producer_id) @param [status]
 **/
class Producerclips extends Scrotis
{

}

/**
 * @package Producerclipsite
 * @method Scrotis show()
 * @method Scrotis getPreviews()
 * @method Scrotis previewsEnabled()
 * @method Scrotis getStoreDir()
 * @method Scrotis getTitle()
 * @method Scrotis getStoreDetails()
 * @method Scrotis isUpdateNeeded()
 * @method Scrotis checkTitle()
 * @method Scrotis ids()
 * @method Scrotis getAutoTweet()
 * @method Scrotis updateAutoTweet()
 * @method Scrotis getProducerTributeCommission()
 * @method Scrotis insertProducerTributeCommission()
 * @method Scrotis updateProducerTributeCommission()
 * @method Scrotis idleStores()
 * @method Scrotis getCategories()
 * @method Scrotis getOutgoingLinks()
 * @method Scrotis getClips()
 * @method Scrotis getActiveStores()
 * @method Scrotis searchActiveStore($pid)
 **/
class Producerclipsite extends Scrotis
{

}

/**
 * @package Producermessage
 * @method Scrotis show()
 * @method Scrotis create()
 * @method Scrotis update()
 * @method Scrotis markAsRead()
 * @method Scrotis markAllAsRead()
 * @method Scrotis createBroadcast()
 * @method Scrotis deleteBroadcast()
 **/
class Producermessage extends Scrotis
{

}

/**
 * @package Producers
 * @method Scrotis show($producer_id = null)
 * @method Scrotis basePath($producer_id)
 * @method Scrotis isHotelProducer($producer_id)
 * @method Scrotis byEmailforPasswordReset($producer_id = null)
 * @method Scrotis byEmailOrIDforPasswordReset($producer_id)
 * @method Scrotis exists($producer_id)
 * @method Scrotis linkCandidates($producer_id)
 * @method Scrotis link($producer_id)
 * @method Scrotis videoImages($producer_id)
 * @method Scrotis getByUsername($producer_id)
 * @method Scrotis getIDbyUsername($param = 1)
 * @method Scrotis getLimitedProducer($producer_id)
 * @method Scrotis limitedUserGroups($limited_user_id)
 * @method Scrotis getWatchListPayhold($producer_id)
 * @method Scrotis getProducerPayhold($producer_id)
 * @method Scrotis producerPayHold($producer_id = null)
 * @method Scrotis getCountryValidationBypass($producer_id)
 * @method Scrotis feedbackCount($producer_id)
 * @method Scrotis unreadFeedbackCount($producer_id)
 * @method Scrotis feedback($producer_id)
 * @method Scrotis totalPay($producer_id)
 * @method Scrotis isUpdateKeyValid($producer_id)
 * @method Scrotis getProducerUpdateByKey($producer_id)
 * @method Scrotis tempProducer()
 * @method Scrotis getDataforRecentlyUpdatedList($producer_id)
 * @method Scrotis hasHtmlLimit($producer_id)
 * @method Scrotis limitProducerHtml($producer_id = null)
 * @method Scrotis producerMadeBanners($producer_id)
 * @method Scrotis elapsedVideoOrders($producer_id)
 * @method Scrotis videoShipping($producer_id)
 * @method Scrotis videoBypassExists($producer_id)
 * @method Scrotis producerImageSite($producer_id)
 * @method Scrotis isProducerImageSiteUpdated($producer_id)
 * @method Scrotis isProducerVideoSiteUpdated($producer_id)
 * @method Scrotis getImageHash($producer_id)
 * @method Scrotis producerPayout($producer_id)
 * @method Scrotis payoutHistory($producer_id)
 * @method Scrotis producerPayoutCount($producer_id)
 * @method Scrotis orderDetails($producer_id)
 * @method Scrotis securityQuestion($pkid = 1)
 * @method Scrotis idImage($id = null)
 * @method Scrotis signupNotification()
 * @method Scrotis itemSite()
 * @method Scrotis badStores()
 * @method Scrotis closedStores()
 * @method Scrotis emailValidate($producer_id)
 * @method Scrotis byEmail()
 * @method Scrotis byPayable()
 * @method Scrotis byCity()
 * @method Scrotis bySS() @param [ss]
 * @method Scrotis badProducerBySS() @param [ss]
 * @method Scrotis byEIN()
 * @method Scrotis byAddress()
 * @method Scrotis isClosed($producer_id)
 * @method Scrotis isI4sV4sClosed($producer_id)
 * @method Scrotis forwarded($producer_id)
 * @method Scrotis isCountryBlocked($producer_id)
 * @method Scrotis blockedCountries($id)
 * @method Scrotis payoneerAccountId($producer_id)
 * @method Scrotis payoneerAccount($producer_id)
 * @method Scrotis trackProducerLogin()
 * @method Scrotis tributeIcon()
 * @method Scrotis redisData($producer_id)
 * @method Scrotis redisClipCount($producer_id)
 * @method Scrotis monthlySalesVolume($producer_id)
 * @method Scrotis hitsLastMonth($producer_id)
 * @method Scrotis hitsThisMonth($producer_id)
 * @method Scrotis unreadBroadcastMessage($producer_id)
 * @method Scrotis unreadBroadcastMessages($producer_id)
 * @method Scrotis byUsernameandID($producer_id)
 * @method Scrotis existsForIDandUsername($producer_id) @param [username]
 * @method Scrotis usernameExists($producer_id)
 * @method Scrotis latestEmails($producer_id)
 * @method Scrotis emailHistory($producer_id)
 * @method Scrotis canViewSalesReportPersonalInfo($producer_id)
 * @method Scrotis tributeIcons($producer_id)
 * @method Scrotis preShippedVideo($producer_id)
 * @method Scrotis relatedProducers($producer_id)
 * @method Scrotis encryptedBlobExists($producer_id)
 * @method Scrotis suggestion()
 * @method Scrotis passwordHistory()
 * @method Scrotis passwordHistoryExists($producer_id)
 * @method Scrotis changePassword($producer_id)
 * @method Scrotis storagePreference($producer_id)
 * @method Scrotis storeProducerVideos($video_id)
 * @method Scrotis getMerchantSignupCount()
 * @method Scrotis producerMerchantActivationHistory()
 * @method Scrotis getClosedStudioCount()
 * @method Scrotis getClosedStores()
 * @method Scrotis getBadStores()
 * @method Scrotis signupAndMerchantActivationData()
 * @method Scrotis tributeRevenueReport()
 * @method Scrotis totalTributeRevenueReport()
 * @method Scrotis sentNotificationsByProducer($producer_id)
 * @method Scrotis countryValidationByPass($producer_id)
 * @method Scrotis notes($pid)
 * @method Scrotis analyzeProducerData($pid)
 * @method Scrotis producerIds($producer_id)
 * @method Scrotis storePaymentAmount()
 * @method Scrotis producerPayoutHistory($producer_id)
 * @method Scrotis totalChargeBacksAndRefunds($pid)
 * @method Scrotis badProducerReason()
 * @method Scrotis producerAmountResult()
 * @method Scrotis earliestAndRecentSales($pid)
 * @method Scrotis merchantData($pid)
 * @method Scrotis unverifiedAccountsList($producer_id)
 * @method Scrotis approve($producer_id)
 * @method Scrotis deny($producer_id)
 * @method Scrotis producerAccounts()
 * @method Scrotis paymentData($pid)
 * @method Scrotis producerClipSiteData($pid)
 * @method Scrotis producersForAnalyze()
 * @method Scrotis producerMerchantAndAccount($pid)
 * @method Scrotis relatedProducerIdsV2()
 * @method Scrotis totalPaymentInformation()
 * @method Scrotis producerTotalPayment($pid)
 * @method Scrotis blockedCountriesDuplicates($producerClipSiteBlockedCountryID)
 * @method Scrotis getPayable($id)
 * @method Scrotis getTitleByPayable($id) @param [payable]
 * @method Scrotis existsInWatchList($producer_id)
 * @method Scrotis producerStoreChanges($pid) @param [page, per_page, from, to]
 * @method Scrotis adminLoginReport($pid) @param [ip, user, from, to, page, limit, grouped]
 * @method Scrotis clipsMissingCategories()
 * @method Scrotis getProducerDetailsByTransID($transId)
 * @method Scrotis updateNotified($producer_id)
 * @method Scrotis getBankAccountByPayable($id)
 * @method Scrotis getProducersByIdOrPayable($id) @param [payable]
 * @method Scrotis getHistoryByID($id)
 * @method Scrotis workerLog($producer_id)
 * @method Scrotis imageMembers() @param [so_number]
 * @method Scrotis videoMembers() @param [so_number]
 * @method Scrotis pendingSignupsCount() @param [so_number]
 * @method Scrotis chargeBacks($producer_id) @param [page, per_page, start_date, end_date, output_type, charge_back_type]
 * @method Scrotis searchProducers() @param [pid, user_name, name, address, city, state, zip, phone, email, payable, page, size]
 * @method Scrotis salesSummaryReport($producer_id) @param [start_date, end_date]
 * @method Scrotis salesSummaryCount($producer_id) @param [start_date, end_date]
 * @method Scrotis updateShipDate($producer_id) @payload [TransID, ShipDate]
 * @method Scrotis getShippedOrders($producer_id) @param [CustomerID]
 * @method Scrotis getPreShippedOrders($producer_id)
 * @method Scrotis salesCategoryReport($producer_id)
 * @method Scrotis salesReport($producer_id)
 * @method Scrotis salesReportCount($producer_id)
 * @method Scrotis searchSales($producer_id)
 * @method Scrotis getMonthSales($producer_id)
 * @method Scrotis salesCustomerReport($producer_id)
 * @method Scrotis salesDetailReport($producer_id)
 * @method Scrotis countTrafficStatistics($producer_id)
 * @method Scrotis countDistinctIPTrafficStatistics($producer_id)
 * @method Scrotis latestTrafficStatistic($producer_id)
 * @method Scrotis countTrafficStatisticsforLastMonth($producer_id)
 * @method Scrotis countTrafficStatisticsforThisMonth($producer_id)
 * @method Scrotis tributeReport($producer_id)
 * @method Scrotis unsoldClips($producer_id)
 * @method Scrotis unsoldClipsCount($producer_id)
 * @method Scrotis refundsReport($producer_id)
 * @method Scrotis featuredClip($fid = null) @param [ClipID]
 * @method Scrotis getActiveFeaturedClipCount($producer_id)
 * @method Scrotis getActiveFeaturedClips($producer_id) @param [limit]
 * @method Scrotis getFeaturedClipIDByClipID($producer_id) @param [clip_id]
 * @method Scrotis getRandomFeaturedClips($producer_id) @param [limit]
 * @method Scrotis badProducers()
 * @method Scrotis badProducerByEmail() @param [email]
 * @method Scrotis badProducerByPayable() @param [payable]
 * @method Scrotis badProducersByCity() @param [city]
 * @method Scrotis badProducerByEin() @param [ein]
 * @method Scrotis badProducerByAddress() @param [address]
 * @method Scrotis getAllproducerMerchantHistory() @param [keyword, per_page, page, sort_field, sort_order]
 * @method Scrotis getProducerByPayed() @param [payed, keyword, per_page, page, sort_field, sort_order]
 * @method Scrotis forwardedProducer($forwarding_id = null) @payload [ProducerID, ForwardTo] @param [id, controller, method]
 * @method Scrotis producerMetaData() @param [pid, key, type]
 * @method Scrotis upcomingTweets() @param [pid, page, size]
 * @method Scrotis producerPayholdLog() @param [studio_id, from, to, page, per_page]
 * @method Scrotis producerMerchantLog() @param [studio_id, from, to, page, per_page]
 * @method Scrotis relatedProducersV2($pid) @param [studio_id, from, to, page, per_page]
 * @method Scrotis detailsTax() @param [detail_id, tax_type, page, size]
 * @method Scrotis tributesTax() @param [tribute_id, tax_type, page, size]
 * @method Scrotis chargebackTotalsData($trans_id)
 * @method Scrotis chargeBackCount($trans_id)
 * @method Scrotis paybackCount($trans_id)
 * @method Scrotis insertProducerDetails($trans_id)
 * @method Scrotis insertIntoRefundChargebackLog($trans_id) @payload [title, type, username, user_ip]
 * @method Scrotis sumPrice($trans_id) @param [detail_ids]
 * @method Scrotis producerDetails($trans_id) @param [detail_ids]
 * @method Scrotis getProducerByRequest() @param [ids, user_name, name, address, city, state, zip, phone, email, payable, country, fax, website, acc_type, addr2257, ss, ein, active, pay_method, bank_name, bank_acc, bank_acc_type, bank_routing, swift_code, bank_country, bank_extra, inter_bank_name, inter_bank_state, inter_bank_country, inter_bank_routing, payout, videop, imagep, clipp, itemid, gizmop, gizmovideop, gizmoimagep, campp, gizmocampp, page, size]
 * @method Scrotis refundTransactionDetails($trans_id) @param [cancelled_refund]
 * @method Scrotis updateProducerDetails($trans_id) @payload [DetailID, CustomerID, Title, Price, TransDate, ProducerID, ProducerCommission, TransID, ShipDate, TrackNum, ClipID, ImageID, VideoID, VidFormat, ItemID, g]
 * @method Scrotis producerDetailsCount($trans_id) @payload [detail_ids]
 * @method Scrotis producerDetailsWithNegativePrice($trans_id) @payload [detail_ids]
 * @method Scrotis insertLog()
 * @method Scrotis updateProducerDetailsWithNegativePrice() @payload [detail_ids]
 * @method Scrotis updateMultipleProducersByID() @param [ids]
 * @method Scrotis nonRefundProducerDetails($trans_id)
 * @method Scrotis producerPayHoldBulk() @payload [ProducerID, Notes, DateTimeAdded, AddedBy, AmountToHold, HoldAll]
 * @method Scrotis insertDetails() @payload [ProducerDetails, CustomerID, Title, Price, TransDate, ProducerID, ProducerCommission, TransID, ShipDate, TrackNum, ClipID, ImageID, VideoID, VidFormat, ItemID]
 * @method Scrotis updateNegativeTaxDetails() @payload [detail_ids]
 * @method Scrotis detailsTaxSum() @param [detail_ids]
 * @method Scrotis negativeIdsForUpdate($trans_id) @param [detail_ids]
 * @method Scrotis getProducerDataWithClipSite($store_id)
 * @method Scrotis clipsListing($pid) @param [page, size]
 * @method Scrotis getProducersTotalBundles($producer_id)
 * @method Scrotis getProducerIDsByEmail() @param [email]
 * @method Scrotis producerPayoutHistoryByProducerId($producer_id)
 * @method Scrotis legalEntityType($id = null)
 * @method Scrotis studiosWithLocations() @param [page, size, vt]
 * @method Scrotis producersByPayableAndIds() @param [payable, ids, page, size]
 * @method Scrotis getAllBlockedCountries() @param [producer_id, country, region, city, from, to, page, per_page]
 * @method Scrotis totalGeoBlockedStudios()
 * @method Scrotis totalGeoBlockedStudiosLocations()
 * @method Scrotis producerPayModificationsLog() @param [pid, page, size]
 * @method Scrotis totalTransactionsByStudios() @param [keyword, from, to, page, per_page]
 * @method Scrotis clipsReport() @param [category, from, to, page, per_page]
 * @method Scrotis clipUploadsAndFirstPurchaseDates() @param [from, to, page, per_page]
 * @method Scrotis getProducerOnlyByKey()
 * @method Scrotis updateidImage()
 * @method Scrotis getidImage()
 * @method Scrotis producerImagesUniqueUserId()
 * @method Scrotis badAndClosedStoresWithCustomNames()
 * @method Scrotis studiosWithActiveClipsCount()
 * @method Scrotis activeAndDuplicateClipsCount()
 * @method Scrotis getNumberStudiosWithCustomPreviews()
 * @method Scrotis getNumberStudiosWithOutCustomPreviews()
 * @method Scrotis getNumberStudioWithGa()
 * @method Scrotis openStoresWithOutClips()
 * @method Scrotis openStoresWithOutCompliance()
 * @method Scrotis clipsWithOutSales()
 * @method Scrotis postingStudiosOrPayABlesCount()
 **/
class Producers extends Scrotis
{

}

/**
 * @package Producervideosite
 * @method Scrotis show()
 * @method Scrotis updateSite()
 * @method Scrotis getShipping()
 * @method Scrotis shipping()
 **/
class Producervideosite extends Scrotis
{

}

/**
 * @package Rabbit
 * @method Scrotis publish()
 **/
class Rabbit extends Scrotis
{

}

/**
 * @package Rapidapi
 * @method Scrotis check()
 **/
class Rapidapi extends Scrotis
{

}

/**
 * @package Rectalogist
 * @method Scrotis sync()
 * @method Scrotis newStoreDir()
 * @method Scrotis removeFile()
 **/
class Rectalogist extends Scrotis
{

}

/**
 * @package Regex
 * @method Scrotis validateImage()
 * @method Scrotis sanitizeImage()
 **/
class Regex extends Scrotis
{

}

/**
 * @package Removedcliphash
 * @method Scrotis getClipId()
 **/
class Removedcliphash extends Scrotis
{

}

/**
 * @package Reportgenerator
 * @method Scrotis getDataForC4S()
 * @method Scrotis getDataForV4S()
 * @method Scrotis getDataForI4S()
 **/
class Reportgenerator extends Scrotis
{

}

/**
 * @package Reports
 * @method Scrotis studiosListing()
 * @method Scrotis customersListing()
 * @method Scrotis salesAndRefunds()
 * @method Scrotis producers()
 * @method Scrotis customers()
 * @method Scrotis groupEmails()
 * @method Scrotis searchGroupEmail()
 * @method Scrotis insertGroupEmail()
 * @method Scrotis insertBulkGroups()
 * @method Scrotis deleteGroupEmails()
 * @method Scrotis checkSent()
 * @method Scrotis insertIntoEmailHistory()
 * @method Scrotis deleteEmailHistoryRecord()
 * @method Scrotis salesTable()
 * @method Scrotis refundsTable()
 * @method Scrotis clipsTable()
 * @method Scrotis clipChartData()
 * @method Scrotis excludedEmails()
 * @method Scrotis excludedIds()
 * @method Scrotis checkExcludedEmail()
 * @method Scrotis insertExcludedEmails()
 * @method Scrotis deleteExcludedEmail()
 * @method Scrotis getProducerSentEmails()
 * @method Scrotis getTopRefferingSites()
 * @method Scrotis getUniquereFerrringSites()
 **/
class Reports extends Scrotis
{

}

/**
 * @package Rolloverimages
 * @method Scrotis getRolloverImageIDByClipId()
 * @method Scrotis addRolloverImage()
 * @method Scrotis deleteRolloverImage()
 **/
class Rolloverimages extends Scrotis
{

}

/**
 * @package Settings
 * @method Scrotis getSetting()
 * @method Scrotis getValue()
 * @method Scrotis signupPolicy()
 * @method Scrotis group()
 **/
class Settings extends Scrotis
{

}

/**
 * @package Signupfields
 * @method Scrotis fieldID()
 * @method Scrotis validateFields()
 * @method Scrotis lastStepID()
 **/
class Signupfields extends Scrotis
{

}

/**
 * @package Signupselectionsoptions
 * @method Scrotis optionLabel()
 * @method Scrotis selectionOptionsByName()
 **/
class Signupselectionsoptions extends Scrotis
{

}

/**
 * @package Signupsteps
 * @method Scrotis show()
 * @method Scrotis stepCount()
 * @method Scrotis options()
 **/
class Signupsteps extends Scrotis
{

}

/**
 * @package Slack
 * @method Scrotis notify() @payload [subject, message, color, channel, username, icon_emoji]
 **/
class Slack extends Scrotis
{

}

/**
 * @package Socialbuttons
 * @method Scrotis show()
 * @method Scrotis getID()
 * @method Scrotis status()
 * @method Scrotis statuses()
 * @method Scrotis tributeReportsSocialButtons()
 * @method Scrotis getNumberOfStudiosWithSocialButton()
 **/
class Socialbuttons extends Scrotis
{

}

/**
 * @package Socialbuttonsdisabled
 * @method Scrotis getSocialButtonsDisabled()
 * @method Scrotis insertSocialButtonsDisabled()
 * @method Scrotis deleteSocialButtonsDisabled()
 **/
class Socialbuttonsdisabled extends Scrotis
{

}

/**
 * @package States
 * @method Scrotis show()
 **/
class States extends Scrotis
{

}

/**
 * @package Storagesync
 * @method Scrotis removedPaths()
 * @method Scrotis fileChanges()
 **/
class Storagesync extends Scrotis
{

}

/**
 * @package Stream
 * @method Scrotis show()
 * @method Scrotis validateToken()
 * @method Scrotis validateAndGetOrderData() @payload [clip_id, ip]
 **/
class Stream extends Scrotis
{

}

/**
 * @package Studio
 * @method Scrotis show($id)
 * @method Scrotis flags()
 * @method Scrotis customStoreName($id = null)
 * @method Scrotis producerClipSite()
 * @method Scrotis clip($studio_id) @payload [clip_id]
 * @method Scrotis topSellingClips($studio_id) @payload [page, per_page]
 * @method Scrotis topClips($producer_id) @payload [page, per_page]
 * @method Scrotis featuredClips($producer_id)
 * @method Scrotis otherMetaData($studio_id)
 * @method Scrotis categories($studio_id)
 * @method Scrotis otherClipStores($studio_id)
 * @method Scrotis otherVideoStores($studio_id)
 * @method Scrotis otherImageStores($studio_id)
 * @method Scrotis otherMemberStores($studio_id)
 * @method Scrotis allOtherStores($studio_id) @payload [storetype]
 * @method Scrotis getId($studio_name)
 * @method Scrotis getIdByNameWithChecking($studio_name)
 * @method Scrotis cc($studio_id)
 * @method Scrotis bankCode($studio_id)
 * @method Scrotis getClipByTitle($producer_id) @payload [title]
 * @method Scrotis socialButtons($studio_id)
 * @method Scrotis merchantSecure($studio_id)
 * @method Scrotis allowOutgoingLinks($studio_id)
 * @method Scrotis trackOutgoingLink() @payload [ProducerID, IPAddress, URL, hit_dttm, type]
 * @method Scrotis producerFeedback() @payload [ProducerID, Title, Description, CustomerEmail]
 * @method Scrotis transactionsInDateRange($studio_id) @payload [start_date, end_date]
 * @method Scrotis producerPay($studio_id) @payload [start_date, end_date]
 * @method Scrotis producerCommission($studio_id)
 * @method Scrotis owed($studio_id)
 * @method Scrotis lastPayoutDate($studio_id)
 * @method Scrotis payoutMetadata($id = null)
 * @method Scrotis broadcastMessage($studio_id)
 * @method Scrotis socialMediaStatus($studio_id)
 * @method Scrotis storeNews($studio_id)
 * @method Scrotis elasticNewStudios($category_id)
 * @method Scrotis redisNewStudios($category_id) @payload [page]
 * @method Scrotis atoz() @payload [letter, page, limit]
 * @method Scrotis atoZStores() @payload [letter, page, limit]
 * @method Scrotis redisAtoZStores() @payload [letter, page, limit]
 * @method Scrotis redisAtoZStoresCount() @payload [limit]
 * @method Scrotis redisRecentlyUpdatedStudios() @payload [category_id, page, limit]
 * @method Scrotis categoryKeywordClipCount($category_id)
 * @method Scrotis redisRecentlyUpdatedStudiosKeywordClipCount() @payload [category_id]
 * @method Scrotis redisRecentlyUpdatedStudioCount() @payload [category_id]
 * @method Scrotis updatedCategoryRank() @payload [studio_id]
 * @method Scrotis elasticRecentlyUpdated() @payload [category_id, page, limit]
 * @method Scrotis redisRecentlyUpdated() @payload [category_id, page, limit]
 * @method Scrotis rank($studio_id)
 * @method Scrotis topStudios($category_id) @payload [days, page, per_page]
 * @method Scrotis elasticTopStudios() @payload [category_id]
 * @method Scrotis search($studio_id)
 * @method Scrotis clips($studio_id)
 * @method Scrotis elasticStudio($studio_id)
 * @method Scrotis studioSearch() @payload [keyword, catid, sortby, sortdir, startlimit, endlimit, titleonly]
 * @method Scrotis getStudioNameByStudioID($studio_id)
 * @method Scrotis studiosWithClip($categoryID) @payload [ids]
 * @method Scrotis reindex($studio_id)
 * @method Scrotis pcsReindex($studio_id)
 * @method Scrotis salesReindex($studio_id)
 * @method Scrotis getStudiosFromRedisByID() @payload [producer_ids]
 * @method Scrotis producerMerchantList($producer_id = null)
 * @method Scrotis getMerchantByProducerId($producer_id)
 * @method Scrotis getUnlinkedProducers() @payload [keyword, sort_field, sort_order]
 * @method Scrotis producerClipSitesWithFirstClipsAdded($siteid)
 * @method Scrotis newStudio($producer_id = null) @payload [ProducerID, StudioName, LatestClipID, ClipCount, CategoryID, UpdateDateTime, RecordCreated, Category]
 * @method Scrotis deleteOldStudios() @payload [limit, categoryid]
 * @method Scrotis customNames() @payload [page, per_page, keyword]
 * @method Scrotis enqueueNewStore()
 * @method Scrotis reSync($studio_id)
 * @method Scrotis reCatch($studio_id)
 * @method Scrotis badCustomName($id = null)
 * @method Scrotis tblLinkCheckerStoresNotToCheck($id = null) @param [sort_field, sort_order]
 * @method Scrotis payoutMetadataByKey() @payload [producer_id, key, value]
 * @method Scrotis clipsForMultiDownloadTool($studio_id) @param [id, page, size, keyword, category_id, extension, clip_active, order, sort, transform]
 * @method Scrotis categoryRank($studio_id)
 * @method Scrotis studioBlockedPageData($studio_id)
 * @method Scrotis pullEmailList($studio_id) @param [page, per_page]
 * @method Scrotis getDoNotLinkProducers() @param [keyword, sort_field, sort_order]
 * @method Scrotis adminAccessBlocked() @payload [ProducerID, Payable, AddedBy, Reason]
 * @method Scrotis customStoreNameLog() @param [csn_id, action, sup_user, page, size] @payload [CustomStoreNameID, Action, SupportUser]
 * @method Scrotis withCountryBlocks()
 * @method Scrotis getStudioBlockedLocations()
 **/
class Studio extends Scrotis
{

}

/**
 * @package Suggestions
 * @method Scrotis show()
 * @method Scrotis redis()
 **/
class Suggestions extends Scrotis
{

}

/**
 * @package Support
 * @method Scrotis authenticate()
 * @method Scrotis workers()
 * @method Scrotis createContact()
 * @method Scrotis getContact()
 * @method Scrotis updateContact()
 * @method Scrotis deactivateContact()
 * @method Scrotis deleteContact()
 * @method Scrotis getPhoneLines()
 * @method Scrotis getWorkerEmailByWorkerName()
 * @method Scrotis userSessionData() @payload [user, session_start, session_end] @param[user, from, to, sort, order, page, limit]
 * @method Scrotis workersNames()
 * @method Scrotis clipExchangeLog() @param [page, per_page, worker_name, start_date, end_date]
 * @method Scrotis getWorkersGroups() @param [worker_id, group_id, page, size]
 * @method Scrotis addUserToGroup() @payload [GroupsID, WorkerID]
 * @method Scrotis deleteUserFromGroup($id)
 **/
class Support extends Scrotis
{

}

/**
 * @package Tempsignup
 * @method Scrotis show()
 * @method Scrotis getUserData()
 * @method Scrotis getID()
 * @method Scrotis tempProducerID()
 * @method Scrotis fieldValue()
 * @method Scrotis getAll()
 * @method Scrotis findStep()
 * @method Scrotis byID()
 **/
class Tempsignup extends Scrotis
{

}

/**
 * @package Token
 * @method Scrotis show() @payload [key]
 * @method Scrotis exists() @payload [key]
 **/
class Token extends Scrotis
{

}

/**
 * @package Topclips
 * @method Scrotis byProducer($producer_id)
 **/
class Topclips extends Scrotis
{

}

/**
 * @package Traffic
 * @method Scrotis showTables()
 * @method Scrotis c4sliveReferrers($producer_id)
 * @method Scrotis forToday($producer_id)
 * @method Scrotis total($producer_id)
 * @method Scrotis C4slivefortoday($producer_id)
 * @method Scrotis totalsFromStatisticalTotals($producer_id)
 * @method Scrotis getReferrers($producer_id)
 * @method Scrotis getReferrersReport($producer_id)
 * @method Scrotis weeklyTrafficSum($producer_id) @param [store_type]
 * @method Scrotis referrersByTLD($producer_id) @param [store_type, from, to]
 * @method Scrotis referrersByTLDUsingDailyTables($producer_id)
 * @method Scrotis getUniques($producer_id) @param [store_type, from, to, limit]
 * @method Scrotis getUniquesUsingDailyTables($producer_id) @param [store_type, from, to, limit]
 * @method Scrotis getHitsforDate($producer_id) @param [date]
 * @method Scrotis topReferers($producer_id)
 * @method Scrotis topUsers($producer_id)
 * @method Scrotis lastReferers($producer_id)
 * @method Scrotis studiosForDate() @param [date, ids]
 * @method Scrotis traffic() @param [from, to]
 * @method Scrotis hitsEventsES() @param [from, to]
 **/
class Traffic extends Scrotis
{

}

/**
 * @package Transaction
 * @method Scrotis getRecentTransactionCountForCC() @payload [cc, minutes_to_go_back]
 * @method Scrotis transactionsBySONumber() @param [so_number]
 * @method Scrotis getTransactionWithCustomerDataBySONumber() @param [so_number]
 * @method Scrotis checkTransaction($producer_id) @param [trans_id]
 * @method Scrotis transaction($producer_id) @param [trans_id]
 * @method Scrotis transactionAddress($producer_id) @param [trans_id]
 * @method Scrotis lastTransaction($producer_id)
 * @method Scrotis transactionDate($transaction_id) @param [producerid]
 * @method Scrotis getTransactionForClipOrderSearch($transaction_id) @param [so_number, clip_is]
 * @method Scrotis tax() @param [so_number, page, size]
 * @method Scrotis refundedTransactions($so_number)
 * @method Scrotis partiallyRefundedTransactions($so_number) @param [reversal]
 * @method Scrotis reversedTransactions($so_number)
 * @method Scrotis updateTransaction($so_number) @payload [CustomerID, DateOfTransaction, Service, TotalAmount, MerchantAmount, VendorComission, NetAmount, cc]
 * @method Scrotis insertTransaction() @payload [CustomerID, DateOfTransaction, Service, TotalAmount, MerchantAmount, VendorComission, NetAmount, cc]
 * @method Scrotis nonRefundTransactionDetails($so_number)
 * @method Scrotis insertIntoTransactionDetails() @payload [ID, Customer_ID, Title, Price, TransDate, VCode, VCommission, Trans, ClipID, VideoID, WornID]
 * @method Scrotis statsByState() @param [from, to]
 * @method Scrotis customerLastPurchase($producer_id) @param [customer_id]
 * @method Scrotis ccBreakdownStats() @param [from, to, sort, order, country, cc]
 * @method Scrotis salesTotal($producer_id) @param [from, to, country, cc]
 * @method Scrotis refundsAndChargebacksByType() @param [type, filter_type, from, to]
 * @method Scrotis returningcustomerswithoutuseraccount()
 * @method Scrotis relatedthumbnails()
 **/
class Transaction extends Scrotis
{

}

/**
 * @package Transcoding
 * @method Scrotis processes()
 * @method Scrotis processCount()
 * @method Scrotis recent() @param [ip, minutes]
 * @method Scrotis recentProcesses() @param [ip, minutes]
 * @method Scrotis otherProcessIds()
 * @method Scrotis queue()
 * @method Scrotis queued()
 * @method Scrotis isQueuedPreview()
 * @method Scrotis recentQueueErrors()
 * @method Scrotis pendingClip()
 * @method Scrotis settings()
 * @method Scrotis serverDetails()
 **/
class Transcoding extends Scrotis
{

}

/**
 * @package Tweet
 * @method Scrotis show()
 * @method Scrotis status($id)
 * @method Scrotis find() @param [params]
 * @method Scrotis tweetSchedule()
 * @method Scrotis clip($clip_id)
 * @method Scrotis getByClipID($clip_id)
 * @method Scrotis galleryID($clip_id)
 * @method Scrotis nextMigrateID()
 * @method Scrotis pending($producer_id)
 * @method Scrotis recentMembersOnly($producer_id)
 * @method Scrotis startedJobsCount($producer_id)
 * @method Scrotis incrementPriority($producer_id) @param [increment]
 * @method Scrotis getUnstartedMigrationJobs() @param [limit]
 * @method Scrotis setStartTimeToNow($transcoding_queue_id)
 * @method Scrotis saveToQueue($producer_id)
 * @method Scrotis addItemsToQueue() @payload [clips]
 * @method Scrotis getPendingPreviewsCountByProducerId($producer_id)
 **/
class Transcodingqueue extends Scrotis
{

}

/**
 * @package Tweet
 * @method Scrotis show()
 * @method Scrotis addTweetSchedule() @payload [Tweet, ScheduledTime, Status, TweetImage, TwitterResponse, ProducerID, TweetTime]
 * @method Scrotis tweetIdLike()
 * @method Scrotis tweetLike()
 * @method Scrotis tweetProducerLike()
 * @method Scrotis producerTweetSchedule($producer_id) @payload [Tweet, ScheduledTime, Status, ProducerID, TweetImage, TwitterResponse]
 * @method Scrotis tweetSchedule() @payload [TweetSchedule, Tweet, ScheduledTime, Status, TweetImage, TwitterResponse, ProducerID, TweetTime]
 * @method Scrotis tweetCountLike()
 * @method Scrotis tweetExistsLike()
 * @method Scrotis tweetCountProducerLike()
 * @method Scrotis TweetExistsProducerLike()
 **/
class Tweet extends Scrotis
{

}

/**
 * @package Twitterserviceconf
 * @method Scrotis config()
 * @method Scrotis findProducersWithTweetsEnabled()
 * @method Scrotis generateScheduledTweets()
 **/
class Twitterserviceconf extends Scrotis
{

}

/**
 * @package Twofactor
 * @method Scrotis test()
 * @method Scrotis auth()
 * @method Scrotis validate()
 * @method Scrotis callback()
 * @method Scrotis sendSms()
 **/
class Twofactor extends Scrotis
{

}

/**
 * @package Unpostedfile
 * @method Scrotis show()
 * @method Scrotis count()
 * @method Scrotis getID()
 * @method Scrotis deleteFiles()
 * @method Scrotis insertFiles()
 * @method Scrotis postedClipIds()
 * @method Scrotis fileBy()
 * @method Scrotis deleteIDs()
 * @method Scrotis rfsExists()
 * @method Scrotis canDelete()
 * @method Scrotis unpostedFiles($producer_id) @param [page, per_page, keyword]
 * @method Scrotis unpostedFile($producer_id) @param [page, per_page, keyword]
 **/
class Unpostedfile extends Scrotis
{

}

/**
 * @package Uploader
 * @method Scrotis authenticate()
 * @method Scrotis limitedUser()
 * @method Scrotis clip()
 * @method Scrotis image()
 **/
class Uploader extends Scrotis
{

}

/**
 * @package Url
 * @method Scrotis isblocked()
 **/
class Url extends Scrotis
{

}

/**
 * @package V4s
 * @method Scrotis topStores()
 * @method Scrotis topSellingVideos()
 * @method Scrotis moreGreatStores()
 * @method Scrotis recentlyUpdatedStores()
 * @method Scrotis newVideoStores()
 * @method Scrotis getTitleById()
 **/
class V4s extends Scrotis
{

}

/**
 * @package V4sdataupdate
 * @method Scrotis nonActiveVideos()
 * @method Scrotis videoCatForUpdatedVideoStore()
 * @method Scrotis newVideoStoresData()
 * @method Scrotis updatedVideoStoresData()
 * @method Scrotis topVideoStoresData()
 * @method Scrotis topVideosData()
 **/
class V4sdataupdate extends Scrotis
{

}

/**
 * @package Videos
 * @method Scrotis show()
 * @method Scrotis getVideoImages()
 * @method Scrotis deleteVideo()
 * @method Scrotis formats()
 * @method Scrotis isDuplicateTitle()
 * @method Scrotis orderDelivered()
 * @method Scrotis orderExists()
 * @method Scrotis hasInvalidTrackingInfo()
 * @method Scrotis hasDeliveryTimeExpired()
 * @method Scrotis OrderTracking()
 * @method Scrotis videoOrder()
 * @method Scrotis videoTrackingNumber()
 * @method Scrotis insertUpdateTrackingID()
 * @method Scrotis getVideos()
 * @method Scrotis getFirstActive()
 * @method Scrotis getVideoSite()
 * @method Scrotis getStudioTopSellingVideos()
 * @method Scrotis getAllVideos()
 * @method Scrotis getAllVideoStores()
 * @method Scrotis searchVideos()
 * @method Scrotis searchStores()
 * @method Scrotis featuredStoreBanner()
 * @method Scrotis getVideoIDs() @param [VideoIDs]
 * @method Scrotis tracking($tracking_id = null) @param [per_page, page, keyword, sort_field, sort_order, trans_date, producer_id, order_number, trans_date]
 * @method Scrotis trackingBypass($store_id = null) @param [per_page, page, keyword, sort_field, sort_order]
 * @method Scrotis pendingManualTracking() @param [per_page, page, keyword, sort_field, sort_order]
 * @method Scrotis createOrUpdateTracking()
 * @method Scrotis updateTracking()
 * @method Scrotis countVideosByProducerId($producer_id)
 * @method Scrotis lastUpdatedVideoByProducerId($producer_id)
 **/
class Videos extends Scrotis
{

}

/**
 * @package CommerceErrorCode
 * @method Scrotis getall()
 **/
class CommerceErrorCode extends Scrotis
{

}

/**
 * @package WLUserClips
 * @method Scrotis config($id)
 * @method Scrotis addUserClips()
 * @method Scrotis findUserClipIDs()
 * @method Scrotis findUserClips()
 **/
class WLUserClips extends Scrotis
{

}

/**
 * @package WLCustomers
 * @method Scrotis getUserClips() @param [clipID]
 * @method Scrotis generateLink() @param [clipID, clipMemberID]
 **/
class WLCustomers extends Scrotis
{

}

/**
 * @package WLStudios
 * @method Scrotis config($id)
 * @method Scrotis studios($id)
 * @method Scrotis relatedStudios($id) @param [studio_id, useCache]
 * @method Scrotis updateStudioCache($id)
 **/
class WLStudios extends Scrotis
{

}

/**
 * @package WLClips
 * @method Scrotis config($id)
 * @method Scrotis categories($id)
 * @method Scrotis modelClipsCount($id)
 * @method Scrotis findClips($id)
 * @method Scrotis findClipsCount($id)
 * @method Scrotis autoComplete($id)
 * @method Scrotis checkClips($id)
 * @method Scrotis relatedModels($id)
 **/
class WLClips extends Scrotis
{

}

/**
 * @package WLSites
 * @method Scrotis getSite($id) @param [name, domain]
 * @method Scrotis updateThemeParams($id)
 * @method Scrotis cleanCache($id)
 **/
class WLSites extends Scrotis
{

}

/**
 * @package WLModels
 * @method Scrotis config($id)
 * @method Scrotis findModels($id)
 * @method Scrotis getModelsStatues($id)
 **/
class WLModels extends Scrotis
{

}

/**
 * @package Taxlocation
 * @method Scrotis getTaxByGeoLocation() @param [Country, StateProvince, County, City, ZipCode, Join, per_page]
 * @method Scrotis createLocation($id = null) @payload [Name, DisplayTaxDuringUserExperience, Country, StateProvince, County, City, ZipCode]
 * @method Scrotis getCleanedUp()
 * @method Scrotis deleteLocBY($id) @param [cascade, ids]
 * @method Scrotis getVatCountries()
 **/
class Taxlocation extends Scrotis
{

}

/**
 * @package Taxratesmeta
 * @method Scrotis createMeta($locID = null) @payload [TaxLocationId, Type, MetaKey, MetaValue, MetaUpdated]
 * @method Scrotis getMetaBy($locID) @param [Type, MetaKey]
 * @method Scrotis deleteMetaBY($id)
 **/
class Taxratesmeta extends Scrotis
{

}

/**
 * @package RightSidePromotion
 * @method Scrotis rightSidePromotion($id = null) @payload [RightSidePromotion, PromotionID, DestinationURL, ImageURL, ImageCAT, DateCreated, DateActiveFrom, DateActiveTill, Activ,]
 * @method Scrotis getPromotionDataForDataTable($id)
 **/
class RightSidePromotion extends Scrotis
{

}

/**
 * @package Gdpr
 **/
class Gdpr extends Scrotis
{

}

/**
 * @package ProducerBannerFileName
 * @method Scrotis producerBannerFileName() @payload [ProducerID, Filename]
 **/
class ProducerBannerFileName extends Scrotis
{

}

/**
 * @package Tracker
 * @method Scrotis producerBannerFileName() @payload [ProducerID, Filename]
 **/
class Tracker extends Scrotis
{

}

/**
 * @package Studiodestroy
 * @method Scrotis studioDeleteDetails() @param [studio_id, user, type, sort, order, page, size]
 * @method Scrotis studioDeleteFiles() @param [action_id, folder, file, page, size]
 * @method Scrotis createStudioDeleteDetail() @param [studio_id, user, type, reason, created_at, updated_at]
 * @method Scrotis createStudioDeleteFiles() @param [studio_action_id, file, folder] @payload [files]
 **/
class Studiodestroy extends Scrotis
{

}

/**
 * @package DuplicateClips
 **/
class DuplicateClips extends Scrotis
{

}

/**
 * @package Faunus
 * @method Scrotis getSeoData($id)
 * @method Scrotis pushSeoData($id)
 **/
class Faunus extends Scrotis
{

}


/**
 * @package Orderimport
 **/
class Orderimport extends Scrotis
{

}

/**
 * @package Bannedwords
 * @method Scrotis example() @payload [ProducerID]
 **/
class Bannedwords extends Scrotis
{

}

/**
 * @package UserAccounts
 * @method Scrotis insertClipPurchaseHistory() @payload [so_number, user_id]
 * @method Scrotis wishlist() @param [page, limit]
 * @method Scrotis clips() @param [page, limit, keyword, sort_by, sort_dir]
 * @method Scrotis getCart()
 * @method Scrotis setcart() @payload [cart]
 * @method Scrotis removeCart()
 * @method Scrotis wishlistForStudioPage() @param [producer_id]
 * @method Scrotis addWishlist() @payload [clip_id, producer_id]
 * @method Scrotis removeWishlist() @payload [clip_id]
 * @method Scrotis associatedEmails() @param [page, per_page]
 * @method Scrotis importOrdersRequest() @payload [email]
 * @method Scrotis associateUserEmail() @payload [email, host]
 * @method Scrotis verifyEmail() @payload [activation_code]
 * @method Scrotis searchUser() @param [search]
 * @method Scrotis emailBySONumber() @param [so_number]
 * @method Scrotis searchUsersByFilters() @param [email, username, first_name, last_name, email_verified, phone_number, country, state, text_updates, zip_code, blocked, page, size, searchUsersByFiltersAction]
 * @method Scrotis updateUserAccount() @payload [user_id, username, first_name, last_name, phone_number, country, state, text_updates, zip_code]
 * @method Scrotis deleteUserAccount($user_id)
 * @method Scrotis blockedUsersAccounts() @payload [id, blocked, blocked_by, note, created_at, updated_at]
 * @method Scrotis unlinkAndDeactivateAccountClips() @payload [email, check_for_blocked_user, user_id, check_for_blocked_user]
 * @method Scrotis hasPromoTransaction() @param [from, to, cid]
 * @method Scrotis removeMultipleClipsFromWishlist() @payload [clip_ids]
 * @method Scrotis verifiedUsersCountByDateRange() @param [from, to]
 * @method Scrotis unverifiedUsersCountByDateRange() @param [from, to]
 * @method Scrotis topSpendingUsers()
 * @method Scrotis topSpendingUserRC() @param [email, type]
 * @method Scrotis deleteCardInfo($user_id)
 * @method Scrotis reimport() @payload [email]
 * @method Scrotis userOrderBasicData($user_id) @param [email, studio_id, clip_id, so_number, page, limit]
 * @method Scrotis newFromUsersFavouriteStudios()
 * @method Scrotis deleteAccountClips() @payload [user_id, clip_ids]
 * @method Scrotis batchUserAccounts() @payload [months, size, page]
 * @method Scrotis followedStudiosClips() @payload [studio_ids, date]
 * @method Scrotis followedStudios() @payload [size]
 * @method Scrotis usersordersbasicdata() @param [not working]
 * @method Scrotis newfromfavouritestudios() @param [not working]
 * @method Scrotis purchasedStudios() @param [not working]
 * @method Scrotis followedStudiosUpdates() @param [not working]
 **/
class UserAccounts extends Scrotis
{

}

/**
 * @package ZipCodeBase
 **/
class ZipCodeBase extends Scrotis
{

}

/**
 * @package ProducerClipMetaData
 * @method Scrotis store()
 * @method Scrotis update($id)
 * @method Scrotis show($id)
 * @method Scrotis destroy($id)
 * @method Scrotis getProducerClipMetaDataByClipIdAndKey() @param [clip_id key]
 **/
class ProducerClipMetaData extends Scrotis
{

}

/**
 * @package Address2257
 * @method Scrotis show($address2257_id = null)
 * @method Scrotis getByStudioId($producer_id)
 * @method Scrotis updateByStudioId($producer_id)
 * @method Scrotis detailsWithFilters($producer_id) @param [pid page size]
 **/
class Address2257 extends Scrotis
{

}

/**
 * @package CheckOutErrorCodes
 * @method Scrotis show($id = null) @param [code] @payload [Code Message Changed_By]
 **/
class CheckOutErrorCodes extends Scrotis
{

}

/**
 * @package BadwordsLog
 * @method Scrotis show() @param [page size key value] @payload [bad_word_id reason created_at created_by]
 * @method Scrotis deleteByBadWordID($bad_word_id)
 **/
class BadwordsLog extends Scrotis
{

}

/**
 * @package ChangeLogger
 * @method Scrotis storeProducerClips() @param [from, to, per_page]
 **/
class ChangeLogger extends Scrotis
{

}

/**
 * @package ComplianceDetails
 * @method Scrotis getByStudioId($producer_id)
 * @method Scrotis updateByStudioId($producer_id)
 * @method Scrotis detailsWithFilters() @param [pid, payable, page, size]
 * @method Scrotis complianceStats()
 **/
class ComplianceDetails extends Scrotis
{

}

/**
 * @package Performer
 * @method Scrotis performers()
 * @method Scrotis addFileToPerformerAction()
 * @method Scrotis getPerformersByStudioId($id) @param [per_age, page, order_by, order_direction]
 * @method Scrotis getPerformersFullInfo($studio_id) @param [per_age, page, order_by, order_direction]
 * @method Scrotis getPerformerFullInfo($performer_id)
 * @method Scrotis deletePerformerFromClip()
 * @method Scrotis addPerformerToClip()
 * @method Scrotis updatePerformerInClips()
 * @method Scrotis updateperformer()
 * @method Scrotis getfilesbyperformerid()
 * @method Scrotis isnameunique()
 * @method Scrotis getperformersbyproduceridcount()
 * @method Scrotis performer()
 **/
class Performer extends Scrotis
{

}

/**
 * @package Tos
 **/
class Tos extends Scrotis
{

}

/**
 * @package CCPA
 * @method Scrotis addCAAgent()
 * @method Scrotis addCAResident()
 * @method Scrotis getCCPAField() @param [key]
 * @method Scrotis addCCPAField()
 * @method Scrotis residentData() @param [email, c4s_email, page, size]
 * @method Scrotis agentData() @param [agent_email, consumer_email, consumer_c4s_email, page, size]
 * @method Scrotis updateResident($id)
 * @method Scrotis updateAgent($id)
 **/
class CCPA extends Scrotis
{

}

/**
 * @package StudioFollows
 * @method Scrotis getBy()
 * @method Scrotis getByCount()
 * @method Scrotis topFollowedStudios()
 * @method Scrotis markASseen()
 * @method Scrotis topFollowedStudiosClips()
 * @method Scrotis getNumberOfStudiosWhoEnablesAutoTweets()
 **/
class StudioFollows extends Scrotis
{

}

/**
 * @package Compliance
 * @method Scrotis complianceClip() @param [clip_id]
 * @method Scrotis insertComplianceClip()
 * @method Scrotis updateComplianceClip($id)
 * @method Scrotis producerClips() @param [producer_id]
 * @method Scrotis clipStatus() @param [clip_id]
 * @method Scrotis insertClipStatus()
 * @method Scrotis updateClipStatus($id)
 * @method Scrotis clipAuditLog() @param [clip_id]
 **/
class Compliance extends Scrotis
{

}

/**
 * @package TopCategoriesByCountry
 **/
class TopCategoriesByCountry extends Scrotis
{

}

/**
 * @package TopClipsByCountry
 **/
class TopClipsByCountry extends Scrotis
{

}
/**
 * @package NewCms
 * @method Scrotis pages()
 * @method Scrotis pageMetaData()
 **/
class NewCms extends Scrotis
{

}



