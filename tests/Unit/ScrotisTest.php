<?php


namespace C4S\Tests;


use C4S\API;
use Orchestra\Testbench\TestCase;
use C4S\Scrotis;

class ScrotisTest extends TestCase
{

    /** @test */
    public function getHostTest() {
        API::setHost('http://scrotis.test');
        $res = Scrotis::Utilities()->getHost();
        $this->assertStringContainsString('http://scrotis.test',$res);
        $this->assertTrue(true);
    }

    /** @test */
    public function getTweetsTest() {
        $res = (array)Scrotis::Utilities()->getTweetsToPost()->get()->first();
        $this->assertArrayHasKey('id',$res);
        $this->assertArrayHasKey('Tweet',$res);
        $this->assertArrayHasKey('ScheduledTime',$res);
        $this->assertArrayHasKey('Status',$res);
        $this->assertArrayHasKey('ProducerID',$res);
        $this->assertArrayHasKey('TweetImage',$res);
        $this->assertArrayHasKey('TwitterResponse',$res);
        $this->assertArrayHasKey('TweetTime',$res);
    }

}