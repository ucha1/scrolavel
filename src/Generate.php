<?php

use Illuminate\Support\Str;

require __DIR__."/../vendor/autoload.php";
require __DIR__."/Scrotis.php";
define('CONTROLLER_PATH', __DIR__.'/../../scrotis-laravel/app/Http/Controllers/');
define('CORE_PATH', __DIR__.'/../../scrotis-laravel/app/Core/Tracker/');

// get the scrotis controllers
$getDiffEndpoints = collect(scanDirS(CONTROLLER_PATH))->map(
    function ($file) {
        $baseName = pathinfo($file, PATHINFO_BASENAME);
        $class    = str_replace('Controller.php', '', $baseName);
        $dirName  = pathinfo($file, PATHINFO_DIRNAME);
        if (CONTROLLER_PATH.'Producers' === $dirName | CONTROLLER_PATH.'ModelAdmin' === $dirName) {
            return null;
        }
        if (in_array($class, ['Error','RightSidePromotion',
            'CustomerMetrics', 'StoreMetrics','ClipMetrics',
            'Ofac', 'Preflight','Data',
            'StoreProducerMetaData','User','Routes','Tests',''])) {
            return null;
        }


        try {
            $rc = new ReflectionClass("\C4S\\$class");
        } catch (Exception $e) {
            require_once CONTROLLER_PATH.'/Controller.php';
            require_once CONTROLLER_PATH.'/PreflightController.php';
            require_once CORE_PATH.'/Support/CanPaginateServiceData.php';
            require_once CORE_PATH.'/Services/ManagementTool.php';
            require_once $file;
            $dirLast = collect(explode('/', pathinfo($file, PATHINFO_DIRNAME)))->last();
            $last    = str_replace('.php', '', collect(explode('/', $file))->last());
            $rc      = new ReflectionClass("App\Http\Controllers\\$dirLast\\$last");
        }

        preg_match_all('/(@method Scrotis) (.+)/', $rc->getDocComment(), $pM);

        $pEndpoints = collect(preg_replace('/\(.*\)/', '', $pM[2]))->map(
            function ($item) {
                return strtolower($item);
            }
        )->toArray();


        $content = file_get_contents($file);
        if (!preg_match_all('/^\s+\* ((GET|DELETE|POST|PUT):? (.+))$/m', $content, $m)) {
            return null;
        }


        return ['controller' => $class, 'endpoint' => $m[1], 'pendpoint' => $pEndpoints];
    }
)->filter()->map(
    function ($item) {
        $arr = [];
        foreach ($item['endpoint'] as $line) {
            [$request_method, $endpoint] = preg_split('/\s+/', trim($line));
            $endpoint = strpos($endpoint, '?') !== false ? trim(parse_url($endpoint, PHP_URL_PATH)) : $endpoint;
            $endpoint = str_replace('/:nid/', '/1/', $endpoint);
            // clean endpoints containing []
            if (Str::contains($endpoint, '[')) {
                $endpoint = preg_replace('/^([^[]+).*$/', '$1', $endpoint);
            }

            if (Str::contains($endpoint, '*')) {
                $endpoint = Str::replaceArray('*', [''],$endpoint);
            }

            // clean endpoints containing {:attribute}
            if (Str::contains($endpoint, '{:attribute}')) {
                continue;
            }

            // clean endpoints containing {:attribute}
            if (Str::contains($endpoint, 'returns')) {
                continue;
            }

            $arr[] = strtolower($endpoint);
        }


        foreach ($item['pendpoint'] as $k => $pLine) {
            if (Str::contains($pLine, ['@param', '@payload'])) {
                $item['pendpoint'][$k] = preg_replace('/\s@.*\s.*/', '', $pLine);
            }
        }
        $item['endpoint'] = $arr;
        return $item;
    }
)->map(
    function ($item) {
        foreach ($item['endpoint'] as $k => $i) {
            if (preg_match('/\/(.*)\/(.*)\/(.*)\/.*/', $i)) {
                $item['endpoint'][$k] = '';
            }
            else {
                $item['endpoint'][$k] = preg_replace('/(\/(.*)\/(.*)\/)|\/(.*)\/(.*)/', '', $i);
            }

            // extra filter in case of & and = chars
            if (str($i)->contains(['&','='])){
                $item['endpoint'][$k] = preg_replace('/(\/(.*)\/(.*)\/)|\/(.*)\/(.*)/', '', str($i)->before('&'));
            }
        }

        $item['endpoint'] = array_filter($item['endpoint']);
        $item['diff']     = array_diff($item['endpoint'], $item['pendpoint']);
        return $item;
    }
)->map(
    function ($item) {
        if (!empty($item['diff']) && !in_array('show', $item['diff'])) {
            addMethodGenerator($item['controller'], $item['diff']);
            return $item['controller']."Controller - ".implode(' | ', $item['diff']);
        }

        return null;
    }
)->filter()->flatten();


function scanDirS($directory): array
{
    $Directory = new RecursiveDirectoryIterator($directory);
    $Iterator  = new RecursiveIteratorIterator($Directory);
    $Regex     = new RegexIterator($Iterator, '/^.+\.php$/i', RecursiveRegexIterator::GET_MATCH);
    $dir       = [];
    foreach ($Regex as $k => $v) {
        $dir[] = $k;
    }

    return $dir;
}

/**
 * at the same time add already scanned methods
 * @param $controller
 * @param $diff
 */
function addMethodGenerator($controller, $diff){
    $docs = (new ReflectionObject(\C4S\Scrotis::{$controller}()));

    $methods = collect($diff)->map(function ($item) {
        return " * @method Scrotis ".$item."()".PHP_EOL;
    });

    $newDoc = Str::replaceLast("**/", $methods->implode('')."**/", $docs->getDocComment());


    $lines = implode("\n", [$docs->getStartLine()-$methods->count(), $docs->getStartLine()]);
    // need a good solution to inject this data between proper file lines
    //file_put_contents($docs->getFileName(), $methods->implode(''),$lines);
}


if ($getDiffEndpoints->isEmpty()) {
    dump("There is no new endpoints form Scrotis");
} else {
    dump($getDiffEndpoints->toArray());
}
