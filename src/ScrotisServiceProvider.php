<?php


namespace C4S;


use Illuminate\Support\ServiceProvider;

class ScrotisServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register() {
        $this->app->singleton(Scrotis::class, function ($app) {
            return new Scrotis();
        });
    }

}