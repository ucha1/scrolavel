# Scrotis Api Explorer
### This is a Scrotis Laravel package,which will help you to fluently navigate through Scrotis endpoints.
### Sort of like Laravel Facades


---
*You can use it in `non Laravel` projects as well!*
===



## Instalation 
inside laravel application directory, 
edit `composer.json` file and add following url in repositories section:
```json
 "repositories": [
    {
      "type": "git",
      "url": "git@gitlab.com:tropicalsun/nonpci/scrolavel.git"
    },
    ...
  ],
```

After that run following command:

```shell script
composer require c4s/scrolavel
```

This should install the package. After finishing installation you can use
it like these: 

```php

// Inside routes/web.php

use C4S\Scrotis;

Route::get('/', function () {
    
    return Scrotis::Utilities()->getTweetsToPost()->get()->pluck("Tweet");
    // Or you could execute it as a raw get request like so:
    return Scrotis::exec("GET","/utilities/0/getTweetsToPost")->pluck("Tweet");

    // Or
    //here I'm passing ProducerID 1587 to get this kind of endpoint:
    //   /producerclips/1587/getTotalClips
    return Scrotis::Producerclips()->getTotalClips('1587')->get();


    // Or enother example
    return Scrotis::Badwords()->getAllBadWords()->get(['page' => 2,'per_page' => 2]);
    
    //Inserting example passing data
    // You can apply same principle for put and delete methods too.
    $response = Scrotis::Utilities()->insertFirstClipAdded()->post([
        "SiteID" => "Test", 
        "DateAndTimeFirstClipAdded" => "Test", 
        "MerchantAccount" => "Test", 
        "ReasonNotLinked" => "Test"
    ]);
    
    
});

```

## Info
- Every return values is an Collection where you could chain your code and continue 
execution on a one line.
- you could pass through array to `get()`,`post()`,`put()` and `delete()` methods accordingly.






