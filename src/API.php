<?php

namespace C4S;

/**
 * Class API
 * a Scrotis API curl helper class
 */
class API
{

    /** Default Host
     *
     * @var string
     */
    private static $host = '';


    private static $headers = array();

    /**
     * Object is not instantiable.
     */
    private function __construct() {
    }

    /**
     * Object should't be cloneable
     */
    private function __clone() {
    }

    /**
     * @param string $host
     * @return string
     */
    public static function setHost(string $host): string {
        return self::$host = $host;
    }

    /**
     * @return string
     */
    public static function getHost() {
        return self::$host;
    }

    /**
     * get header info
     * make a HEAD request
     *
     * @param $endpoint
     * @return object
     */
    public static function head($endpoint) {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::$host . $endpoint);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        /** @noinspection CurlSslServerSpoofingInspection */
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        /** @noinspection CurlSslServerSpoofingInspection */
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_exec($ch);

        $info = curl_getinfo($ch);

        curl_close($ch);

        return (object)$info;
    }

    public static function get($url, $obj = array()) {
        return self::exec('GET', $url, $obj);
    }

    /**
     * @param string $url
     * @param array  $obj
     * @return mixed
     */
    public static function post($url, $obj = array()) {
        return self::exec('POST', $url, $obj);
    }

    /**
     * @param string $url
     * @param array  $obj
     * @return mixed
     */
    public static function put($url, $obj = array()) {
        return self::exec('PUT', $url, $obj);
    }

    /**
     * @param string $url
     * @param array  $obj
     * @return mixed
     */
    public static function delete($url, $obj = array()) {
        return self::exec('DELETE', $url, $obj);
    }

    /**
     * API::poolGet(['endpoint1','endpoint2'])
     * Batch Get request
     *
     * @param       $endpoint
     * @param array $options
     * @return array
     * @internal param $endpoint
     * @internal param bool $type
     */
    public static function poolGet(array $endpoint, $options = array()) {

        $curly = array();
        $result = array();
        $mh = curl_multi_init();

        foreach ($endpoint as $id => $d) {
            $curly[$id] = curl_init();
            $url = self::$host . $d;
            curl_setopt($curly[$id], CURLOPT_URL, $url);
            curl_setopt($curly[$id], CURLOPT_HEADER, 0);
            curl_setopt($curly[$id], CURLOPT_HTTPHEADER, self::$headers);
            curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);
            if (!empty($options)) {
                curl_setopt_array($curly[$id], $options);
            }
            curl_multi_add_handle($mh, $curly[$id]);
        }

        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while ($running > 0);

        foreach ($curly as $id => $c) {
            $result[$id] = curl_multi_getcontent($c);
            curl_multi_remove_handle($mh, $c);
        }

        curl_multi_close($mh);

        return $result;
    }

    /**
     * API::poolPost([
     *      ['/endpoint1',['CategoryID' => '13','ProducerID' => '1587']]
     *      ['/endpoint2',['CategoryID' => '13','ProducerID' => '1587']]
     * ]);
     * Batch Post request
     *
     * @param       $endpoint
     * @param array $options
     * @return array
     */
    public static function poolPost(array $endpoint, $options = array()) {

        $curly = array();
        $result = array();
        $mh = curl_multi_init();

        foreach ($endpoint as $id => $d) {

            $curly[$id] = curl_init();
            $url = (!empty($d[1]) && is_array($d[1])) ? self::$host . $d[0] : '';
            curl_setopt($curly[$id], CURLOPT_URL, $url);
            curl_setopt($curly[$id], CURLOPT_HEADER, 0);
            curl_setopt($curly[$id], CURLOPT_HTTPHEADER, self::$headers);
            curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curly[$id], CURLOPT_POST, 1);
            curl_setopt($curly[$id], CURLOPT_POSTFIELDS, json_encode($d[1]));

            if (!empty($options)) {
                curl_setopt_array($curly[$id], $options);
            }

            curl_multi_add_handle($mh, $curly[$id]);
        }

        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while ($running > 0);

        foreach ($curly as $id => $c) {
            $result[$id] = curl_multi_getcontent($c);
            curl_multi_remove_handle($mh, $c);
        }

        curl_multi_close($mh);

        return $result;
    }

    /**
     * set headers
     *
     * @param array $arr
     * @return string
     */
    public static function withHeaders($arr = array()) {
        self::$headers = array_merge(array('Accept: application/json', 'Content-Type: application/json'), $arr);

        return get_called_class(); // static::class; php 5.5 only
    }


    /**
     * set Authorization Bearer token
     *
     * @param $param
     * @return string
     */
    public static function withToken(array $param)
    {
        // init expected vars
        $username = "";
        $password = "";
        $token = "";
        extract($param, EXTR_IF_EXISTS);

        if (!empty($token)) {
            self::withHeaders(["Authorization: Bearer ".$token]);
        } elseif (empty($token) && !empty($username) && !empty($password)) {
            //get token
            self::withHeaders(["Content-Type: application/x-www-form-urlencoded"]);
            $token = collect(self::post("/auth/login/", compact("username", "password")))->get("token");
            // token was provided by user
            unset(self::$headers[2]);
            self::withHeaders(["Authorization: Bearer ".$token]);
        }

        return get_called_class(); // static::class; php 5.5 only
    }

    /**
     * Combine execution bby case
     *
     * @param  string  $method
     * @param  string  $endpoint
     * @param  array|string  $body
     * @return mixed
     */

    public static function exec($method, $endpoint, $body) {
        $curl = curl_init();

        switch ($method) {
            case 'GET':
                if (strrpos($endpoint, '?') === false) {
                    $endpoint .= '?' . http_build_query($body);
                }
                break;

            case 'POST':
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, is_array($body) ? json_encode($body) : $body);
                break;

            case 'PUT':
            case 'DELETE':
            default:
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($method)); // method
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($body)); // body
        }

        curl_setopt($curl, CURLOPT_URL, self::$host . $endpoint);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, self::$headers);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);

        // Exec
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        return json_decode(substr($response, $info['header_size']));
    }

}
